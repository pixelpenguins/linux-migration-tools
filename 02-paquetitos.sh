#!/bin/bash

#me quedé por k3b

echo "INSTALANDO BÁSICOS"
apt-get install synaptic aptitude clamav clamtk clamav-freshclam dconf-editor debian-keyring encfs firmware-linux-nonfree firmware-misc-nonfree gtk-update-icon-cache gtk2-engines-pixbuf gvfs gvfs-fuse hp-ppd hpijs-ppds hplip hplip-gui printer-driver-hpcups hp-ppd hplip hplip-data intel-gpu-tools intel-microcode ksystemlog multiarch-support ntfs-3g ntp ntpdate openjdk-8-jdk openjdk-8-jre openprinting-ppds opensc opensc-pkcs11 openssh-client parted gparted printer-driver-all reportbug smbclient wine wine64 winetricks alien alsamixergui apt-show-versions apt-xapian-index furiusisomount fuse fuseiso fuseiso9660 gconf-editor gdisk gksu rsync grsync gzip bash-completion battery-stats alien alsa-base alsa-oss alsa-tools alsa-tools-gui alsa-utils alsamixergui appstream apt apt-file apt-listbugs apt-utils apt-xapian-index aptdaemon aptdaemon-data aptitude cups cups-browsed cups-bsd cups-client cups-common cups-core-drivers cups-daemon cups-filters cups-filters-core-drivers cups-pdf cups-pk-helper cups-ppdc cups-server-common deborphan default-java-plugin default-jre default-jre-headless discover gnome-disk-utility htop dfc net-tools preload mtools system-config-printer unetbootin unetbootin-translations usb-modeswitch usbutils wget xclip xdg-user-dirs xdg-user-dirs-gtk xdg-utils xserver-xorg-video-intel

# Virtualbox
apt-get install virtualbox virtualbox-qt virtualbox-dkms

# Para equipos portátiles
apt-get install laptop-mode-tools task-laptop

echo "ok"

echo "AÑADIENDO ARQUITECTURA i386"
dpkg --add-architecture i386
echo "ok"


echo "INSTANDO XFCE"
apt-get install gigolo gtk2-engines-xfce gtk3-engines-xfce xfce4-datetime-plugin xfce4-notifyd xfce4-terminal exo-utils orage parole thunar thunar-archive-plugin thunar-data thunar-media-tags-plugin thunar-volman tumbler xfce-keyboard-shortcuts xfce4-appfinder xfce4-artwork xfce4-battery-plugin xfce4-clipman xfce4-clipman-plugin xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-diskperf-plugin xfce4-goodies xfce4-indicator-plugin xfce4-linelight-plugin xfce4-mailwatch-plugin xfce4-messenger-plugin xfce4-mount-plugin xfce4-netload-plugin xfce4-notes xfce4-notes-plugin xfce4-panel xfce4-places-plugin xfce4-power-manager xfce4-pulseaudio-plugin xfce4-quicklauncher-plugin xfce4-radio-plugin xfce4-screenshooter-plugin xfce4-sensors-plugin xfce4-settings xfce4-smartbookmark-plugin xfce4-systemload-plugin xfce4-taskmanager xfce4-timer-plugin xfce4-verve-plugin xfce4-volumed xfce4-wavelan-plugin xfce4-whiskermenu-plugin xfce4-weather-plugin xfce4-xkb-plugin xfconf xfdesktop4 xfwm4 alacarte
echo "ok"



echo "INSTALANDO XFCE EXTRAS"
apt-get install compton compton-conf compton-conf-l10n lxappearance lxtask shared-mime-info wicd wicd-daemon wicd-gtk gamin menulibre mousepad ristretto lxrandr gdebi hamster-applet breeze breeze-cursor-theme cryptkeeper dmz-cursor-theme galculator hardinfo clipit gtkhash light-locker mugshot pcmanfm pcmanfm-qt pcmanfm-qt-l10n pcmanx-gtk2 software-properties-gtk sound-theme-freedesktop spacefm spacefm-common
echo "ok"



echo "INSTALANDO XFCE EXTRAS MATE"
apt-get install atril engrampa engrampa-common eom  mate-utils pluma caja caja-actions caja-actions-common caja-common caja-extensions-common caja-gksu caja-open-terminal caja-sendto caja-wallpaper mozo oxygen-sounds oxygencursors
echo "ok"



echo "INSTALANDO EXTRAS"
apt-get install gnote gprename kdeadmin systemsettings unzip alacarte arj bleachbit font-manager fontmatrix  p7zip p7zip-full bzip2 cabextract mintbackup
echo "ok"


echo "INSTALANDO FUENTES"
apt-get install fonts-cabin fonts-cantarell fonts-wine fonts-roboto fonts-droid fonts-inconsolata fonts-liberation fonts-lobster fonts-oxygen ttf-bitstream-vera ttf-mscorefonts-installer ttf-ubuntu-font-family
echo "ok"


echo "INSTALANDO HERRAMIENTAS DE INTERNET"
apt-get install filezilla firefox-esr firefox-esr-l10n-es-es firefox-l10n-es httrack owncloud-client owncloud-client-l10n pidgin pidgin-themes rdesktop remmina remmina-plugin-rdp remmina-plugin-vnc vinagre vino axel cclive youtube-dl corebird dc-qt deluge deluge-common deluge-gtk dukto  dvdbackup dvdrip ekiga gpodder steadyflow amule amule-common amule-utils chromium chromium-inspector chromium-l10n icedove xul-ext-gcontactsync icedove-l10n-es-es iceowl-extension iceowl-l10n-es-es liferea midori pidgin pidgin-extprefs pidgin-plugin-pack transmission-gtk uget valknut valknut-translations xul-ext-downthemall xul-ext-flashgot xul-ext-gcontactsync xul-ext-quotecolors xul-ext-webdeveloper xul-ext-tabmixplus xul-ext-flashgot xul-ext-flashblock xul-ext-downthemall 
echo "ok"


echo "INSTALANDO HERRAMIENTAS GRÁFICAS"
apt-get install blender dia dia-common dia-libs dia-shapes fontforge fontforge-extras fotoxx gifsicle glabels gphoto2 hugin hugin-tools inkscape mypaint poster rawtherapee sane sane-utils synfig synfigstudio webp xsane aaphoto agave create-resources darktable dcraw  enblend enfuse fotowall gcolor2 geeqie gimp gimp-data gimp-data-extras gimp-gap gimp-gmic gimp-gutenprint gimp-lensfun gimp-plugin-registry gimp-texturize gimp-ufraw ufraw  gmic gpick gtkmorph icc-profiles icc-profiles-free imagemagick agave autotrace converseen digikam digikam-data digikam-private-libs dispcalgui dvipng exiftran fyre phatch jpegoptim trimage krita krita-l10n luminance-hdr metapixel mypaint mypaint-data mypaint-data-extras optipng pdf2svg photocollage photofilmstrip 
echo "ok"


echo "INSTALANDO HERRAMIENTAS MULTIMEDIA"
apt-get install avidemux avidemux-plugins avidemux-qt devede espeak-ng espeak-ng-data ffmpeg ffmpegthumbs handbrake-gtk handbrake-cli kdenlive kdenlive-data kodi kodi-bin kodi-data libavcodec57 libavfilter6 libavutil55 libavutil54 libquicktime2 libswresample2 libswscale4 libxine2 libxine2-bin libxine2-plugins mencoder mixxx mixxx-data mkvtoolnix mkvtoolnix-gui mplayer mpv opus-tools pitivi shotcut shotcut-data shutter simple-scan smplayer smplayer-l10n subdownloader vlc vlc-bin vlc-l10n vlc-plugin-qt vlc-plugin-samba vlc-plugin-base vlc-plugin-pulse x265 aegisub aegisub-l10n ardour ardour-data ardour-video-timeline audacious audacious-plugins:amd64 audacious-plugins-data audacity audacity-data audio-recorder aumix camorama cheese cinelerra-cv cinelerra-cv-data faac ffmpeg2theora frei0r-plugins gaupol gmtp gnome-alsamixer gnome-audio gtk-recordmydesktop gtk-theme-config gtk-theme-switch guvcview imagination ardour flac flashplugin-nonfree growisofs libx264-148 python-mlt bino cmyktool dvdauthor easymp3gain-gtk faad festival festvox-ellpc11k gtkpod lame libcanberra-gtk-module libcanberra-gtk0 libcanberra-gtk3-0 libcanberra-gtk3-module libcanberra0 libcdaudio1 libdca0 libdvdcss2 libfaac0 libmp3lame0 libvlc-bin libvlccore8 libwebp5 mediainfo mediainfo-gui melt mpeg3-utils mppenc openshot openshot-qt oss-compat oxygen-sounds pavucontrol soundconverter soundkonverter sox speech-tools subtitleripper timidity transcode vcdimager vorbis-tools vorbisgain w64codecs winff winff-gtk2 wodim xawtv-plugins xcalib camorama cclive dvd+rw-tools easymp3gain-data easymp3gain-gtk kid3-core kid3-qt media-player-info melt mjpegtools twolame goodvibes
echo "ok"


echo "INSTALANDO HERRAMIENTAS DE DESARROLLO"
apt-get install csstidy geany geany-plugins sqlite3 sqlitebrowser sqlitestudio sqlitestudio-plugins:amd64 bluefish bluefish-data bluefish-plugins emacs24 emacs24-bin-common emacs24-common emacs24-el git git-man gitg gphpedit retext meld 
echo "ok"


echo "INSTALANDO HERRAMIENTAS DE OFICINA"
apt-get install dict-freedict-eng-spa dict-freedict-spa-eng dictionaries-common djview4 impressive libreoffice libreoffice-gtk3 libreoffice-l10n-es libreoffice-java-common libreoffice-nlpsolver libreoffice-ogltrans libreoffice-pdfimport libreoffice-style-sifr  mupdf mupdf-tools pandoc pdf2djvu pstoedit psutils tesseract-ocr uno-libs3 ure zim catdoc catfish fbreader fslint jpegoptim advancecomp anki calendar-google-provider caribou catdoc catfish lyx mcomix pdfmod pdfposter pdfshuffler pdftk stardict-common stardict-gtk stardict-plugin stardict-plugin-espeak stardict-plugin-festival unoconv webkit2pdf

echo "ok"


echo "INSTALANDO JUEGOS"
apt-get install extremetuxracer extremetuxracer-data extremetuxracer-extras  gnuchess gscan2pdf gthumb hedgewars kajongg kbreakout neverball neverputt  mupen64plus mupen64plus-audio-all mupen64plus-input-all mupen64plus-qt mupen64plus-video-all performous photofilmstrip pingus qmc2 qmc2-data stockfish supertux supertuxkart teeworlds tuxpaint boswars crack-attack desmume dosbox fceux fretsonfire fretsonfire-game frozen-bubble frozen-bubble-data gbrainy gcompris gcompris-data gcompris-sound-es gnome-video-arcade higan joystick jstest-gtk zsnes:i386 angrydd crack-attack fofix gnome-games gnome-games-extra-data gnome-video-arcade ioquake3 ioquake3-server visualboyadvance-gtk visualboyadvance kmahjongg ltris mame mame-tools advancemenu megaglest numptyphysics openarena openarena-081-maps openarena-081-misc openarena-081-players openarena-081-players-mature openarena-081-textures openarena-085-data openarena-088-data openarena-data teeworlds trigger-rally warmux warzone2100 warzone2100-music zazx


# Wesnoth
apt-get install wesnoth wesnoth-1.14 wesnoth-1.14-aoi wesnoth-1.14-core wesnoth-1.14-data wesnoth-1.14-did wesnoth-1.14-dm wesnoth-1.14-dw wesnoth-1.14-ei wesnoth-1.14-httt wesnoth-1.14-l wesnoth-1.14-low wesnoth-1.14-music wesnoth-1.12-nr wesnoth-1.12-sof wesnoth-1.14-sotbe wesnoth-1.14-thot wesnoth-1.14-trow wesnoth-1.14-tsg wesnoth-1.14-ttb wesnoth-1.14-utbs wesnoth-core wesnoth-music

# da error:
# apt-get install speed-dreams

echo "ok"

echo "INSTALANDO OTROS"
apt-get install wit ccd2iso espeak fdupes gpxviewer command-not-found kde-config-gtk-style kde-config-gtk-style-preview kinfocenter numix-gtk-theme thewidgetfactory
echo "ok"






# a falta de instalar o en duda
# apt-get install gtk3-nocsd i965-va-driver i965-va-driver:386 photoprint ultrastar-deluxe vdrift vokoscreen xchat
