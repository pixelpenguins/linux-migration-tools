#!/bin/bash

echo "INSTALANDO ARCHIVOS DE CONFIGURACIÓN EN EL HOME"

echo "....."
echo "ATENCIÓN!!! - Antes de ejecutar este script, asegúrate de que estás seguro de hacerlo..."
read KEY


for D in /home/*; do
    if [ -d "${D}" ]; then
        echo "${D}"   
		

		if [ "${D}" == "/home/lost+found" ]; then
			echo "lost+found encontrado... se salta..."
		fi

		cp -rf ./home/* "${D}"
		

		cd ..
		echo
    fi
done


echo "ok"
