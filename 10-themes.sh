#!/bin/bash

echo "INSTALANDO PERSONALIZACIÓN DE ESCRITORIO"

echo "INSTALANDO DEPENDENCIAS"
apt-get install libgdk-pixbuf2.0-dev libglib2.0-bin librsvg2-dev ruby-sass


git clone https://github.com/jEsuSdA/the-perfect-desktop.git

cd ./the-perfect-desktop

cd bashrc
cp /root/.bashrc /root/.bashrc.orig
cp -rf root.bashrc /root/.bashrc
cd ..


cd cursors
7za x breeze_cursors.7z
rm -rf breeze_cursors.7z breeze_cursors.theme
mv -rf breeze_cursors /usr/share/icons/
cd ..

cd fonts
dpkg -i *.deb
apt-get -f install
cd ..

cd greybird-patched
./install-greybird.sh Greybird-20190326.7z
cd ..

cd papirus-patched
dpkg -i papirus-folders_1.3.1-1~76~ubuntu19.04.1_all.deb
apt-get -f install 
./install-papirus.sh papirus-20190523.7z
cd ..

cd sound-theme
apt-get install dconf-tools sound-theme-freedesktop xfconf pavucontrol
apt-get install sox gnome-session-canberra at-spi2-core xdg-utils
./install.sh
cd ..

cd xfwm4-themes
7za x Greybird-kgt-3.7z
mv -rf Greybird-kgt-3 /usr/share/themes/
cd ..





echo "ok"
