#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


	cambiaext "$1"

	gs -sDEVICE=jpeg -dJPEGQ=100 -dNOPAUSE -dEPSFitPage -dBATCH -dSAFER -r300 -sOutputFile="$namefich"jpg "$1"

