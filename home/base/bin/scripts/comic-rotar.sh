#!/bin/bash


#NECESARIOS: imagemagick, rar y xpdf-utils.
# Especialmente dise�ado para convertir a .cbr aquellos pdf's que por el m�todo normal vuelcan las im�genes invertidas.

for i in *.pdf
do

	echo "1-Creando directorio..."
	mkdir "_$i"

# 	dir= echo -n $i | sed -e 's/.pdf//g'

	

  	echo "2-Copiando pdf..."
  	cp "$i" "_$i"
 	cd "_$i" 
  
  	echo "3-Extrayendo im�genes del pdf ..."
  	pdfimages "$i" pic
 	
  	rm "$i"
  
  	echo "4-Convirtiendo im�genes a jpg ..."
  	for e in *.p?m
  	do
  		convert  -quality 50 "$e" "$e.jpg"
		rm "$e"
		convert -rotate "180" "$e.jpg" "$e.jpg"
		convert -flop "$e.jpg" "$e.jpg"
  	done
 
 	
 	cd ..
  
  	echo "5-Comprimiendo directorio ..."	
  	rar a "$i.rar" "_$i"

	rm -r "_$i"
	rm "$i"
  
  	echo "OK!"

done
