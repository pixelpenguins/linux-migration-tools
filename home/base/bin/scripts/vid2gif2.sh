#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}




	origen="$1"
	cambiaext "$1"
	vidout=$namefich"gif"

	mkdir temp 2> /dev/null
	cd temp

	#ffmpeg -i "$origen" -r 5 -vf scale=400:-1 .temp/out%04d.gif
	ffmpeg -i ../"$origen" -vf scale=400:-1 out%04d.gif


	convert -delay 1x20 -loop 0 out*.gif animation.gif 


	# optimizar gif
	convert -layers Optimize animation.gif animation_small.gif 
	gifsicle -O3 --colors 200 "animation_small.gif" > "animation_small2.gif"


	mv animation_small2.gif ../"$vidout"

	cd ..

	rm -rf temp






