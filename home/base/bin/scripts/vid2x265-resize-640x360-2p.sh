#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
vidbitrate=600k

for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv  *.dv *.webm
do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""x265.mkv"


# ffmpeg -i "$origen"  -vcodec libx265 -s 640x360 -b:v "$vidbitrate" -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 -acodec libvorbis -ar 44100 -ab 128k -ac 2 "$vidout"

ffmpeg -i "$origen"  -vcodec libx265 -s 640x360 -b:v "$vidbitrate" -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 -acodec libvorbis -ar 44100 -ab 96k -ac 2 "$vidout"

done 

