#!/bin/bash

# carpetatemp=$$
# 
# mkdir $carpetatemp


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


echo "REQUIERE TENER INSTALADOS ps2pdf"


for i in *.pdf
do

	echo -n "Procesando $i" 

	cambiaext "$i"
	echo -n "."

	mv "$i" "$namefich"bak
	echo -n "."

	pdf2ps "$namefich"bak "$namefich"ps
	echo -n "."
	
	ps2pdf -dOptimize=true "$namefich"ps "$namefich"pdf
	echo -n "."

	rm -rf "$namefich"ps
	echo -n "."

	echo " OK!"

done


# 
# for i in *.pdf 
# do
# 	echo -n "comprimiendo $i .."
# 	pdf2ps "$i" "$i".ps
# 		echo -n "."
# 	mv "$i" "$i".bak
# 		echo -n "."
# 	ps2pdf -dOptimize=true "$i".ps "$i"
# 		echo -n "."
# 	rm -rf "$i".ps
# 		echo "..OK!"
# done
