#!/bin/bash

# ests script lanza algunos servicios necesarios para ejecutar correctamente algunas aplicaciones de KDE en Gnome
# debe ejecutarse como servicio al inicio de sesión de Gnome.


# Notificación de alertas de Korganizer
korgac &

# Cartera de contraseñas de KDE
kdewalletmanager &

# DEMONIO KDE 4
sleep 5 && killall -s 9 kded4 
sleep 5 && killall -s 9 kded4 
sleep 5 && killall -s 9 kded4


# Servidor AKonadi
# akonadiserver &	
