#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}








for i in *.mkv 
# s*.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv  *.dv *.vob *.m4v *.webm

#for i in *.mp4

do 

	origen="$i"
	cambiaext "$i"
	vidtemp="$namefich""tmp.mkv"
	vidtemp2="$namefich""tmp2.mkv"
	vidout="$namefich""x264.mkv"
	bitrate="915k"


	echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k

	echo "el bitrate optimo es $bitrateoptimo"
	echo "codificando a $bitrateoptimo"
	bitrateoptimo=800k


	if [ "$origen" = "$namefich".mkv ]
	then
		echo "Es un archivo MKV"
		# QUITAMOS LOS SUBTÍTULOS
		mkvmerge --no-subtitles "$origen" -o "$vidtemp"
	fi






	# https://trac.ffmpeg.org/wiki/Hardware/VAAPI
	# https://wiki.libav.org/Hardware/vaapi


ffmpeg -vaapi_device /dev/dri/renderD128 -hwaccel vaapi -hwaccel_output_format vaapi -i "$origen" -ac copy -vf 'format=nv12|vaapi,fps=30,hwupload,scale_vaapi=w=1280:h=720' -threads 0 -r 30 -c:v h264_vaapi -profile:v high -level:v 3 -qp 25 -y "$vidout"



	rm -rf x264_2pass.log.mbtree
	rm -rf ffmpeg2pass-0.log
	rm -rf x264_2pass.log
	rm -rf *.log.tmp
	rm -rf *.log.mbtree



done 






