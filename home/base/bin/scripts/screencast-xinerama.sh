#!/bin/sh
# SCREENCAST BY JESUSDA
# SISTEMA DE GRABACIÓN DE SCREENCAST CON SOX, FFMPEG Y MENCODER.


fname=`date +"screencast_%Y-%m-%d_%m-%M_%S"`
begindate=`date +"%s"`

formatovideo=avi


clear

echo "===================================================="
echo 
echo -n "ACTIVANDO GRABACIÓN DE AUDIO... "

# -q oculta la salida de SOX. Para debug, quitar este parámetro.
#sox -q -t alsa "hw:0,0"  -t ogg -A -2 -c1 audio.ogg rate 44100 gain +16 dither -s > /dev/null &

#sox -q -t alsa "hw:1,0"  -t ogg -c1 audio.ogg rate 44100 gain +16 dither -s > /dev/null &
#soxpid=`ps -A | grep sox | awk '{print $1}'`


pacat --record -d alsa_output.pci-0000_00_1b.0.analog-stereo.monitor | sox -t raw -r 44100 -e signed-integer -L -b 16 -c 2 - "audio.wav" > /dev/null &

soxpid=`ps -A | grep sox | awk '{print $1}'`
pacatpid=`ps -A | grep pacat | awk '{print $1}'`

echo "Ok!"
echo
echo "===================================================="
echo 
echo "ACTIVANDO GRABACIÓN DE VÍDEO... Ok!"
echo 
echo "Pulsa la tecla Q para terminar la grabación"
echo
sleep 1


# -loglevel panic oculta la salida de ffmpeg. Para debug, quitar este parámetro.

# resolución antigua: monitores Dell TRC y TFT antiguos.
#ffmpeg -loglevel panic -f x11grab -r 12 -s 1680x1050 -r 12 -i :0.0+1152,0 -qscale 0 video."$formatovideo" || beep && beep

# resolución para grabar el vídeo del monitor DELL grande.
ffmpeg -loglevel panic -f x11grab -r 60 -s 1920x1200 -r 60 -i :0.0+1680,0 -qscale 0 video."$formatovideo" || beep && beep

kill -9 $soxpid 2> /dev/null

echo "===================================================="
echo 
echo "UNIENDO AUDIO Y VÍDEO..."
echo 


# primero cortaremos el primer segundo del audio, ya que como el scrtip comienza a grabar el audio antes que el vídeo, hay siempre un desfase de un segundo en el que el audio va adelantado.

#ffmpeg -y -ss 00:00:01 -i audio.ogg -acodec copy audio_cut.ogg
ffmpeg -y -ss 00:00:01 -i audio.wav -acodec copy audio_cut.wav

mencoder video."$formatovideo" -ovc copy -oac mp3lame -audiofile audio_cut.wav -o "$fname".mkv

#mv audio_cut.ogg "$fname".bak.ogg
mv audio_cut.wav "$fname".bak.wav

mv video."$formatovideo" "$fname".bak."$formatovideo"

#rm audio.ogg video."$formatovideo"
#rm audio.ogg -rf
rm audio.wav -rf




enddate=`date +"%s"`
timediff=`expr $enddate - $begindate`
mins=`expr $timediff / 60`
secs=`expr $timediff % 60`

clear 
echo "===================================================="
echo 
echo "La grabación $fname duró $mins minutos y $secs segundos"
echo 
echo "===================================================="


exit



# http://www.funwithelectronics.com/?id=95

# DOCUMENTATION

# In order to find the correct device you should run this command:

# pacmd list | grep ".monitor"

# When you see something like:

# 	alsa_output.pci-0000_06_05.0.analog-stereo.monitor/#1: Monitor of CA0106 Soundblaster Analog Stereo

# and the soundcard corresponds to the one you want to monitor, you know which device name to use. Here it is: alsa_output.pci-0000_06_05.0.analog-stereo.monitor

# Afterwards pacat is used to read the signal and sox is used to change the output format. See the man page of sox for parameters to choose for correct output format. The default output from pacat is raw audio, rate 44100, signed-integer, little-endian, 16-bit and stereo. The sox reads from standard input by specifying "-" as filename. The format of the output file is therefore specified after the "-" in the examples under.

# Here is an example of how to write to a float formatted raw audio file with mono sound at a rate of 40000sps:

# pacat --record -d alsa_output.pci-0000_06_05.0.analog-stereo.monitor | sox -t raw -r 44100 -s -L -b 16 -c 2 - -t raw -r 40000 -f -b 32 -c 1 output.raw

# This one is nice if you will record sounds for using in gnuradio. To record to a wav-file with a rate of 44100sps, simply do like this:

# pacat --record -d alsa_output.pci-0000_06_05.0.analog-stereo.monitor | sox -t raw -r 44100 -s -L -b 16 -c 2 - "output.wav"



