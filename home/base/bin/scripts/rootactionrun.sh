#!/bin/sh

ACTION=$1
WORKDIR=$2
EXECFILE=$3

case $ACTION in
  runfromhere)
        cd "$WORKDIR"
        kdesu "$EXECFILE"
        exit 0
        ;;

  runinkonsole)
        cd "$WORKDIR"
        KONSOLECMD="konsole --noclose -e $EXECFILE"
        kdesu "$KONSOLECMD"
        exit 0
        ;;


  *)
	exit 1
	;;
esac