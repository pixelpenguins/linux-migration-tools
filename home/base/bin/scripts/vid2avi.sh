#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
# vidbitrate=6000

for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.ogm *.ogv *.dv *.vob
do 

	origen="$i"
	cambiaext "$i"
	vidout=$namefich"avi"

	mv "$origen" "$origen"".bak"

	mencoder "$origen"".bak" -o "$vidout" -ovc xvid -xvidencopts bitrate=1700 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1


#mencoder "$origen" -o "$vidout" -ovc xvid -xvidencopts bitrate=1000 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1




done 

