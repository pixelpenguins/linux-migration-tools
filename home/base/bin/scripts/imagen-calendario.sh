#!/bin/bash
#===========================================
# calendario.sh
# Crea un calendario a partir de una imagen
# uso: calendario-imagen.sh "gato.png"
#===========================================

IMAGEN=$1

if [ -z $IMAGEN ];
then
	echo "Debes indicar una imagen para crear el calendario"
	exit;
fi;

# Edita estos valores, como desees:

# Fuente a utilizar, ojo no todas estan soportadas por imagemagick, usa por ejemplo Courier
#FUENTE="/usr/share/fonts/truetype/ttf-droid/DroidSans.ttf"
FUENTE="Courier"

# Tama�o de la fuente
SIZE=14

#Color de fondo (por lo general es black)
COLOR=gray
# % de Opacidad, transparencia
OPACIDAD=55

# Dimensiones del calendario
DIMENSIONS=600x600
BOLD=0

# Crear el calendario

# Creamos las imagenes del calendario 2010 (1 con transparencia y otra con fondo oscuro)
cal -h 2010 | convert -background transparent -fill white -font $FUENTE -pointsize 12 -strokewidth $BOLD -stroke white -gravity center label:@- calendario2010t.png
cal -h 2010 | convert -background $COLOR -fill black -font $FUENTE -pointsize 12 -strokewidth $BOLD -stroke white -gravity center label:@- calendario2010.png

# Redimensionamos la imagen original
cp $IMAGEN input.png
# Y le aplicamos el primer calendario con cierto grado de opacidad
convert input.png -resize ${DIMENSIONS}! miff:- | composite -dissolve $OPACIDAD% -gravity center calendario2010.png - micalendario.png
# a la composicion resultante le aplicamos ya el otro calendario sin trasnparencias
composite -gravity center calendario2010t.png micalendario.png micalendario.png

# Borramos lo sobrante
rm calendario2010t.png
rm calendario2010.png
rm input.png

exit;

# Si quisieramos crear la marca de agua seria parecido a esto:
#echo "(c) 2010 Ubuntu Life" | convert -background transparent -fill white -pointsize 24 label:@- watermark.png
#convert input.jpg -resize 500x500 miff:- | composite -watermark 30% -gravity center watermark.png - output.png
#convert input.jpg -resize 500x500 miff:- | composite -dissolve 40% -gravity center watermark.png - output.png

