#!/bin/bash

lineas=0



# find . -type f -inum 31467125 -exec cat {} | sed '/^\s*$/d' | wc -l 

php=`find . -iname "*.php" -exec cat "{}" \; | wc -l`
js=`find . -iname "*.js" -exec cat "{}" \; | wc -l`
css=`find . -iname "*.css" -exec cat "{}" \; | wc -l`
xml=`find . -iname "*.xml" -exec cat "{}" \; | wc -l`
html=`find . -iname "*.html" -exec cat "{}" \; | wc -l`


lineas=`expr $php + $js`
lineas=`expr $lineas + $css`
lineas=`expr $lineas + $xml`
lineas=`expr $lineas + $html`

echo "--------------------------------------"
echo "El proyecto tiene: $lineas líneas de código"
echo "--------------------------------------"
echo "PHP: $php líneas de código"
echo "JavaScript: $js líneas de código"
echo "CSS: $css líneas de código"
echo "HTML: $html líneas de código"
echo "--------------------------------------"

