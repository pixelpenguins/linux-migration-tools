#!/bin/bash

# carpetatemp=$$
# 
# mkdir $carpetatemp

pdfimages $1 pic


for i in *.ppm 
do
	convert $i $i.jpg
	mogrify -flip $i.jpg
	rm -rf $i
done

# rm $2

rar a $1.cbr *.jpg
rm -rf *.jpg