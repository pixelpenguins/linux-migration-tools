#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


	echo "REQUIERE TENER INSTALADOS pstoedit y pdf2svg"

	cambiaext "$1"


#	en realidad el script primero pasa de ai a pdf y luego de pdf a svg.

	pstoedit -f gs:pdfwrite "$namefich"ai "$namefich"pdf
	pdf2svg "$namefich"pdf "$namefich"svg

	rm "$namefich"pdf

	echo "OK!"
