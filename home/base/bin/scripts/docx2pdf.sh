#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}

mkdir bak

for i in *.docx *.doc
do

	cambiaext "$i"

	unoconv   --format=pdf "$i" -o "$namefich"pdf
	
	mv "$i" bak/

done 
