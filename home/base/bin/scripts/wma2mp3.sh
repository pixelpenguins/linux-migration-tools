#!/usr/bin/perl


# wma2mp3, a simple script to create mp3 files throug wma ones.

# Original Script Author: unknown (notify me if you know where and who is him).
# Atuthor: jEsuSdA <jesusda@ono.com>
# This software released under the terms of the GPL LICENSE.
# <http://www.opensource.org/licenses/gpl-license.php>

# version 1.2.0 by jEsuSdA.


$dir=`pwd`;
$version = "v1.2.0";

print "wma2mp3 $version\n";
print "(c) 2002-2005 jEsuSdA\n";
print "Released without warranty under the terms of the GPL License\n\n";

## READ EACH FILE, IF WMA, MAKE A "COPY" TO WAV THEN TO MP3 THEN DELETE WMA AND WAV.

chop($dir);

opendir(checkdir,"$dir");

while ($file=readdir(checkdir)) {

$orig_file=$file;

if ($orig_file !~ /\.wma$/i) {next};

print "Checking file: $orig_file\n";


$new_wav_file=$orig_file;$new_wav_file=~s/\.wma/\.wav/;
$new_mp3_file=$orig_file;$new_mp3_file=~s/\.wma/\.mp3/;

$convert_to_wav="mplayer \"./$orig_file\" -ao pcm:file=\"./$new_wav_file\"";
$convert_to_mp3="lame -h \"./$new_wav_file\" \"./$new_mp3_file\"";
$remove_wav="rm -rf \"./$new_wav_file\"";

print "EXEC 1: $convert_to_wav\n";
$cmd=`$convert_to_wav`;
print "EXEC 2: $convert_to_mp3\n";
$cmd=`$convert_to_mp3`;
print "REMOVE WAV: $remove_wav\n";
$cmd=`$remove_wav`;
print "\n\n";

}

print "Done.";


