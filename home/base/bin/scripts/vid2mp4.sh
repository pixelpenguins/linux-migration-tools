#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
# vidbitrate=6000
# for i in *.avi  
for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mov *.rm *.ogg *.ogm *.dv
do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""mp4"


mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts  subq=5:8x8dct:frameref=2:bframes=3:b_pyramid=normal:weight_b -vf unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1 -af resample=44100 -srate 44100


done 

