#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}




for i in *.avi *.mkv
do 

	origen="$i"
	cambiaext "$i"
	vidout=$namefich"mp3"


echo $origen

ffmpeg -i "$origen" -map 0:2 -acodec libmp3lame -ar 44100 -ab 128k -ac 2 "$vidout"

#ffmpeg -i $origen -map 0:1 $vidout

#ffmpeg -i video2 -i audio.mp3 -c:v copy -c:a copy -map 0:v:0 -map 1:a:0 $vidout



done 

