#!/bin/bash


# GENERA UN VIDEO A PARTIR DE UNA IMAGEN

# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}

	origen="$1"
	cambiaext "$1"
	vidout=$namefich"avi"




ffmpeg -loop_input -i "$1" -t 3 "$vidout"






