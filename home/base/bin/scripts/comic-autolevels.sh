#!/bin/bash

QUALITY=75

mkdir bak


for i in *.cbr
do

	echo "ABRIENDO $i"
	mkdir comic

# 	DESCOMPRIMIR EN COMIC
	unrar x "$i" comic

# 	ENTRAR EN COMIC
	cd comic
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS

	find . -type f -name *.jpg  -exec cp {} . \;
	find . -type f -name *.jpeg  -exec cp {} . \;
	find . -type f -name *.JPG  -exec cp {} . \;
	find . -type f -name *.JPEG  -exec cp {} . \;
	find . -type f -name *.db  -exec rm -rf {} \;

# 	CONVERTIR LAS IMÁGENES A MENOS CALIDAD

	echo "RECOMPRIMIENDO IMÁGENES..."
	for b in *.jpg
	do
		im-autolevel.sh -c average "$b" "$b"
	done


# 	SALIMOS DE COMIC Y COMPRIMIMOS EN RAR
	cd ..

	echo "CERRRANDO $i"

	rar a "$i".cbr comic/
	rm -rf comic

done


# 	HACEMOS LO MISMO PARA LOS ZIP

for i in *.zbz
do

	echo "ABRIENDO $i"
	mkdir comic

# 	DESCOMPRIMIR EN COMIC
	unzip "$i" -d comic

# 	ENTRAR EN COMIC
	cd comic
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS

	find . -type f -name *.jpg  -exec cp {} . \;
	find . -type f -name *.jpeg  -exec cp {} . \;
	find . -type f -name *.JPG  -exec cp {} . \;
	find . -type f -name *.JPEG  -exec cp {} . \;

# 	CONVERTIR LAS IMÁGENES A MENOS CALIDAD

	echo "RECOMPRIMIENDO IMÁGENES..."
	for b in *.jpg
	do
		im-autolevel.sh "$b" "$b"

	done


# 	SALIMOS DE COMIC Y COMPRIMIMOS EN RAR
	cd ..

	echo "CERRRANDO $i"

	rar a "$i".cbr comic/
	rm -rf comic
done

mv *.zip bak
mv *.rar bak