#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.ogm *.ogv  *.dv
#for i in *.avi
do 

	origen="$i"
	cambiaext "$i"
	vidout=$namefich"webm"


#	ffmpeg -i "$origen" -f webm -vcodec libvpx -b 900k -bt 900k -acodec libvorbis -ar 44100 -ab 64k -ac 2 -threads 4 "$vidout"
	ffmpeg -i "$origen" -f webm -vcodec libvpx -b 1100k -bt 1100k -acodec libvorbis -ar 44100 -ab 64k -ac 2 -threads 2 "$vidout"

#ffmpeg -i "$origen" -an -pass 1 -passlogfile %~n1 -b 1000 -minrate 0 -maxrate 2000 -bufsize 224 -vcodec libvpx -threads 1 "$vidout"
#ffmpeg -i "$origen" -acodec libvorbis -ar 44100 -ab 64k -ac 2 -pass 2 -passlogfile %~n1 -b 1000 -minrate 0 -maxrate 2000 -bufsize 224  -f webm -vcodec libvpx -threads 1 -y "$vidout"


#	ffmpeg -i "$origen" -pass 1 -an  -f webm -vcodec libvpx -b 800k -bt 800k -threads 1 "$vidout"
#	ffmpeg -i "$origen" -pass 2 -acodec libvorbis -ar 44100 -ab 64k -ac 2 -f webm -vcodec libvpx -b 800k -bt 800k -threads 1 -y "$vidout"





done 

