#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


#algoritmo="convert -monochrome -density 400 -depth 4 "
#algoritmo="convert -colorspace Gray -modulate 100,00,100 -depth 3 -colors 8 -normalize -density 800 "
# algoritmo="convert -normalize -colorspace Gray -modulate 100,00,100 -depth 3 -colors 8 -normalize  -density 200 "

#algoritmo="convert -normalize -colorspace Gray -modulate 115,00,200 -colors 8 -normalize  -density 200 "

#algoritmo="mogrify -format png -auto-level -normalize -modulate 105,00,00 -colorspace gray -colors 9 -auto-level  -normalize  "

#algoritmo="convert -format png -auto-level -normalize -modulate 105,00,00 -colorspace gray -colors 9 -auto-level  -normalize  "

algoritmo="convert -format png  -auto-level -normalize -colorspace gray -modulate 100,00,100 -depth 3 -colors 8 -normalize -density 800 "

algoritmo="convert -format png  -colorspace gray -auto-level -depth 2 -density 800 "


QUALITY=75
tecla=a



echo "PREPARANDO..."

mkdir bak

echo "CAMBIANDO EXTENSIÓN A LOS COMICS..."

for i in *.cbr
do
	origen="$i"
	cambiaext "$i"
	out=$namefich"rar"
	mv "$i" "$out"

done

for i in *.cbz
do
	origen="$i"
	cambiaext "$i"
	out=$namefich"zip"
	mv "$i" "$out"

done


minusculas.sh
minusculas.sh

# cp *.zip bak
# cp *.rar bak




for i in *.rar 
do


	origen="$i"
	cambiaext "$i"
	out=$namefich"cbz"


	echo "ABRIENDO $i"
	mkdir comic

# 	DESCOMPRIMIR EN COMIC
	unrar x "$i" comic


# 	ENTRAR EN COMIC
	cd comic
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS

	find . -type f -name *.jpg  -exec cp {} . \;
	find . -type f -name *.jpeg  -exec cp {} . \;
	find . -type f -name *.JPG  -exec cp {} . \;
	find . -type f -name *.JPEG  -exec cp {} . \;
	find . -type f -name *.db  -exec rm -rf {} \;
	find . -type f -name *.png  -exec cp {} . \;
	find . -type f -name *.PNG  -exec cp {} . \;
	find . -type f -name *.gif  -exec cp {} . \;
	find . -type f -name *.GIF  -exec cp {} . \;

	chmod 777 *.*
	minusculas.sh



# 	CONVERTIR LAS IMÁGENES A 1Bit CALIDAD

	echo
	echo "RECOMPRIMIENDO IMÁGENES a 1bit..."

	mkdir 1bit

	
	for b in *.jpg
	do

		$algoritmo "$b" "$b".png 
		mv  "$b".png 1bit/
		echo -n .
	done



# 	EN ESTA PARTE SE REQUIERE LA AYUDA DEL USUARIO

	echo
	echo "COPIA LAS IMÁGENES A COLOR QUE TE INTERESEN EN LA CARPETA 1BIT y pulsa tecla"
	read "$tecla"
	read "$tecla"


	echo
	echo "OPTIMIZANDO IMÁGENES..."


	cd 1bit

# 	CONVERTIR LAS IMÁGENES A MENOS CALIDAD


	echo "RECOMPRIMIENDO IMÁGENES..."
	for b in *.jpg
	do
		convert -quality $QUALITY "$b" "$b".jpg 2> /dev/null
		rm "$b" 2> /dev/null
		echo -n .
	done

	cd ..
	

# 	Quitamos datos EXIF de las imágenes
	mogrify -strip 1bit/*.png
	mogrify -strip 1bit/*.jpg

# 	Optimizamos los png's
	optipng 1bit/*.png


# 	SALIMOS DE COMIC Y COMPRIMIMOS EN zip



	cd ..

	echo "CERRRANDO $i"

	zip "$out" comic/1bit/*
	rm -rf comic

done

















# 	HACEMOS LO MISMO PARA LOS ZIP

for i in *.zip
do



	origen="$i"
	cambiaext "$i"
	out=$namefich"cbz"
	

	echo "ABRIENDO $i"
	mkdir comic

# 	DESCOMPRIMIR EN COMIC
	unzip "$i" -d comic



# 	DESCOMPRIMIR EN COMIC
	unrar x "$i" comic


# 	ENTRAR EN COMIC
	cd comic
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS

	find . -type f -name *.jpg  -exec cp {} . \;
	find . -type f -name *.jpeg  -exec cp {} . \;
	find . -type f -name *.JPG  -exec cp {} . \;
	find . -type f -name *.JPEG  -exec cp {} . \;
	find . -type f -name *.db  -exec rm -rf {} \;
	find . -type f -name *.png  -exec cp {} . \;
	find . -type f -name *.PNG  -exec cp {} . \;
	find . -type f -name *.gif  -exec cp {} . \;
	find . -type f -name *.GIF  -exec cp {} . \;

# 	CONVERTIR LAS IMÁGENES A MENOS CALIDAD

	minusculas.sh

	echo "RECOMPRIMIENDO IMÁGENES..."
	for b in *.jpg *.png *.gif
	do
		convert -quality $QUALITY "$b" "$b".jpg 2> /dev/null
		rm "$b" 2> /dev/null
		echo -n .
	done

# 	CONVERTIR LAS IMÁGENES A 1Bit CALIDAD

	echo
	echo "RECOMPRIMIENDO IMÁGENES a 1bit..."

	mkdir 1bit

	
	for b in *.jpg
	do

		$algoritmo "$b" "$b".png 
		mv  "$b".png 1bit/
		echo -n .
	done


# 	EN ESTA PARTE SE REQUIERE LA AYUDA DEL USUARIO

	echo
	echo "COPIA LAS IMÁGENES A COLOR QUE TE INTERESEN EN LA CARPETA 1BIT y pulsa tecla"
	read "$tecla"
	read "$tecla"

	echo
	echo "OPTIMIZANDO IMÁGENES..."


# 	Quitamos datos EXIF de las imágenes
	mogrify -strip 1bit/*.png
	mogrify -strip 1bit/*.jpg

# 	Optimizamos los png's
	optipng 1bit/*.png



# 	SALIMOS DE COMIC Y COMPRIMIMOS EN zip



	cd ..

	echo "CERRRANDO $i"

	zip "$out" comic/1bit/*
	rm -rf comic

done





