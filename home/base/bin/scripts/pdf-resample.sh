#!/bin/bash

# primer parámetro = nombre archivo pdf.
# segundo parámetro = páginas.


echo " pdf-resample.sh nombre-archivo paginas"

i=0
	echo -n "convirtiendo pdf a imagenes jpg .."

while [ $i -lt $2 ];
do

	
	convert -density 200% "$1"["$i"]  "$1"_"$i".jpg 
	echo -n "."
	let i=i+1 
done

	echo "..OK!"

	echo -n "convirtiendo imagenes jpg a pdf.."


for i in *.jpg
do
	nice -12 convert  "$i"  "$i".pdf
	rm "$i"
	
done

pdfjoin *.pdf


	echo "..OK!"
