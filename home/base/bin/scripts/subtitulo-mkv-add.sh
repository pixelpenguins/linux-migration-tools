#!/bin/bash

echo "Uso: subtitulo-mkv-extract.sh"



# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}





for i in *.srt 
do

	origen="$i"
	cambiaext "$i"
	vidout=$namefich"x264.sub.mkv"
	vidin=$namefich"x264.mkv"


	echo $origen

	mkvmerge -o "$vidout"   "$vidin"  "$origen"


done
