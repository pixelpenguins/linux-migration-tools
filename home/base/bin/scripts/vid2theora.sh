#!/bin/bash


# ES NECESARIO TENER INSTALADO ffmpeg2theora

# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
# vidbitrate=6000

for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov  *.dv
do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""ogm"

#	ffmpeg2theora -S 2 -v 7 -K 250 "$origen" -o "$vidout"

#	ffmpeg2theora -S 2 -v7 -K 250 --two-pass --optimize "$origen" -o "$vidout"
#	ffmpeg2theora -V 800 -v8 --optimize  -A 64 -c 2 -H 44100 "$origen"

	ffmpeg2theora -V 800 -v8 -K 250 -A 64 -c 2 -H 44100 --two-pass --optimize "$origen" -o "$vidout"

#	ffmpeg2theora -S 2 -speedlevel 0 -V 3500 -K 250 --optimize "$origen" -o "$vidout"

#	-V 512 -A 96
#	-x 640 -y 480
# 	ffmpeg2theora -s 10 -e 120 -V 512 -A 96 x 640 -y 480 --optimize -o nombre-alternativo videoclip.extensión


#tovid + ffmpeg2theora
#En la mayoría de los casos un único paso con ffmpeg2theora suele ser suficiente. Pero si encontramos problemas con el audio, podemos hacer una primera conversión usando tovid.
#Si el vídeo no es de mucha calidad, bastará con la opción -dvd-vcd. Si no, podemos usar -dvd.
#Ejemplos:
#$ tovid -dvd-vcd -pal -full -in original.avi -out destino
#$ tovid -dvd -pal -full -in original.avi -out destino
#Después, se pasa directamente el vídeo resultante destino.mpg a ffmpeg2theora.
#$ ffmpeg2theora destino.mpg



done 

