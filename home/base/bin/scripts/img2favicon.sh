#!/bin/bash


# Genera un icono para web (FAVICON.ico) a partir de una imagen dada.

# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}






	origen="$1"
	cambiaext "$1"
	out=$namefich"ico"

echo "USO: img2favicon.sh <imagen.png>"

convert $origen -define icon:auto-resize=64,48,32,16 $out


echo "OK!"



