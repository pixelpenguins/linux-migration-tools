#!/bin/bash
# **************************************************************************
# Script para, dado un archivo de vídeo por parámetro, generar los formatos
# necesarios para su uso en la web.
# by jEsuSdA 8)


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# **************************************************************************
# Variables
# modificar el tamaño y bitrate a conveniencia.
size=480x270
bitrate=1200k


# generamos el nombre del archivo
origen="$1"
cambiaext "$1"
vidout=$namefich"web"
# forzamos que ffmpeg no muestre su salida por pantalla
log=" -v quiet "


echo "**************************************************************************"
echo "USO: vid2web.sh <video a convertir>"
echo "**************************************************************************"

# **************************************************************************
# Generamos el fivhero WebM: MKV + VP8 + Vorbis.
echo -n "Generando WebM..."

ffmpeg $log -i "$origen"  -f webm -vcodec libvpx  -b $bitrate -acodec libvorbis -ab 64k -ar 44100 -threads 2  "$vidout".webm

echo " Ok!"

# **************************************************************************
# Generamos el fivhero MP4 (en 2 pasadas): MP4 + H264 + AAC.
echo -n "Generando MP4..."


ffmpeg -i "$origen"  -f mp4 -codec:v libx264 -crf 21 -bf 2 -flags +cgop -pix_fmt yuv420p -codec:a aac -strict -2 -b:a 192k -r:a 48000 -movflags faststart     "$vidout".mp4


# ffmpeg $log -y -i "$origen" -f mp4 -c:v libx264 -preset slow -b:v $bitrate -pass 1 -c:a libfaac -b:a 128k -s $size /dev/null

# ffmpeg $log -i "$origen" -f mp4 -c:v libx264 -preset slow -b:v $bitrate -pass 2 -c:a libfaac -b:a 128k -s $size "$vidout".mp4

# rm -rf ffmpeg2pass-0.log.mbtree
# rm -rf ffmpeg2pass-0.log

echo " Ok!"

# **************************************************************************
# Generamos el fivhero OGV: OGG + Theora + Vorbis.
echo -n "Generando OGV..."
ffmpeg $log -i "$origen" -f ogg -vcodec libtheora -b $bitrate -acodec libvorbis -ab 64k -ar 44100 -threads 2   "$vidout".ogv
echo " Ok!"

# **************************************************************************
# Generamos un snapshot.
echo -n "Generando SnapShot..."

# Obtener número de frames del vídeo

## frames=`ffprobe -show_streams "$origen" 2> /dev/null | grep nb_frames | head -n1 | sed 's/.*=//'`

ffmpeg $log -i "$origen" -ss 00:10 -vframes 1 -r 1 -s $size "$vidout".jpg
echo " Ok!"

# **************************************************************************
echo "Job done!"

