#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
# vidbitrate=6000

#for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv
for i in *.avi  *.dv

do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""ogv"


#ffmpeg -i "$origen" -an -pass 1 -vcodec libtheora -b 800k -bt 800k -threads 0 "$vidout"
#ffmpeg -i "$origen" -acodec libvorbis -ar 44100 -ab 64k -ac 2 -pass 2 -vcodec libtheora -b 800k -bt 800k -threads 0  -y "$vidout"

#ffmpeg2theora --first-pass -V 800 --optimize  -A 64 -c 2 -H 44100 "$origen" 
#ffmpeg2theora --second-pass -V 800 --optimize  -A 64 -c 2 -H 44100 "$origen"

ffmpeg2theora -V 800 -v8 --optimize --two-pass -A 64 -c 2 -H 44100 "$origen"



#mencoder "$origen" -o /dev/null -oac copy -ovc lavc -lavcopts vcodec=libtheora vbitrate=800:vhq:vpass=1 
#mencoder "$origen" -o "$vidout" -oac lavc -lavcopts acodec=vorbis br=64:q=7:aq=9:cbr:mode=1 -ovc lavc -lavcopts vcodec=libtheora vbitrate=800:vhq:vpass=2 



rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log





done 

