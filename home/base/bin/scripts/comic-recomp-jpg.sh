#!/bin/bash


function cambiaext2 {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}

function cambiaext {
    str=$1
    namefich=`echo $1 | cut -d . -f 1`
}


function optimizacomic {


# 	ENTRAR EN COMIC
	cd comic
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS

	find . -type f -name *.jpg  -exec cp {} . \;
	find . -type f -name *.jpeg  -exec cp {} . \;
	find . -type f -name *.JPG  -exec cp {} . \;
	find . -type f -name *.JPEG  -exec cp {} . \;
	find . -type f -name *.db  -exec rm -rf {} \;
	find . -type f -name *.png  -exec cp {} . \;
	find . -type f -name *.PNG  -exec cp {} . \;
	find . -type f -name *.gif  -exec cp {} . \;
	find . -type f -name *.GIF  -exec cp {} . \;

# 	CONVERTIR LAS IMÁGENES A MENOS CALIDAD

	chmod 777 *.*
	minusculas.sh

	echo "RECOMPRIMIENDO IMÁGENES..."
		
	mogrify -format jpg *.png
	mogrify -format jpg *.gif

		rm -rf *.gif
		rm -rf *.png


	for b in *.jpg
	do

		NEWNAME=`basename "$b".png`
		echo -e "Sharpening "$b" into $NEWNAME\n\nEdge detect:"
		convert -monitor -edge 2 "$b" orig_edge.png
		echo -e "\nUnsharp original:"
		convert -monitor -unsharp 1x1+1+.01 "$b" temp_sharp.png
		echo -e "\nSoftening edge:"
		convert -monitor -threshold 40% -blur 2 -threshold 30% -blur 2 orig_edge.png soft_edge.png
		echo -e "\nCompositing:"
		composite -monitor temp_sharp.png "$b" soft_edge.png "$b"

		convert -normalize -quality $QUALITY "$b" "$b".jpg
		rm "$b"
		echo -n .

	done

rm -rf *_edge*.png
rm -rf *_sharp*.png
rm -rf *.png.jpg
rm -rf *.png-*.jpg
rm -rf *.gif.jpg
rm -rf *.gif-*.jpg

# 	SALIMOS DE COMIC Y COMPRIMIMOS EN RAR
	cd ..
}



QUALITY=75



echo "PREPARANDO..."
mkdir bak
echo "CAMBIANDO EXTENSIÓN A LOS COMICS..."

for i in *.cbr
do
	origen="$i"
	cambiaext "$i"
	out=$namefich".rar"
	mv "$i" "$out"

done

for i in *.cbz
do
	origen="$i"
	cambiaext "$i"
	out=$namefich".zip"
	mv "$i" "$out"

done


minusculas.sh
minusculas.sh

echo -n "Deseas hacer un backup de los cómics? (Y/N):"
read tecla

if [ $tecla = y ]; then

	cp *.zip bak 2> /dev/null
	cp *.rar bak 2> /dev/null

fi


for i in *.rar
do


	comic="$i"

	origen="$comic"
	cambiaext2 "$comic"
	out=$namefich"cbz"
	comicfin="$out"

	echo "ABRIENDO $origen"
	mkdir comic

# 	DESCOMPRIMIR EN COMIC
	unrar x "$origen" comic


	optimizacomic

	echo "CERRRANDO $comic"

	zip "$comicfin" comic/*
	rm -rf comic
done


for i in *.zip
do

	comic="$i"

	origen="$comic"
	cambiaext2 "$comic"
	out=$namefich"cbz"
	comicfin="$out"


	echo "ABRIENDO $origen"
	mkdir comic

# 	DESCOMPRIMIR EN COMIC
	unzip "$origen" -d comic

	optimizacomic

	echo "CERRRANDO $comic"

	zip "$comicfin" comic/*
	rm -rf comic
	rm -rf /tmp/magick*
	
done













# 	HACEMOS LO MISMO PARA LOS ZIP

#for i in *.zip
#do
#
#	origen="$i"
#	cambiaext "$i"
#	out=$namefich"cbz"
#
#
#	echo "ABRIENDO $origen"
#	mkdir comic
#
# 	DESCOMPRIMIR EN COMIC
#	unzip "$origen" -d comic
#
# 	ENTRAR EN COMIC
#	cd comic
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS
#
#	find . -type f -name *.jpg  -exec cp {} . \;
#	find . -type f -name *.jpeg  -exec cp {} . \;
#	find . -type f -name *.JPG  -exec cp {} . \;
#	find . -type f -name *.JPEG  -exec cp {} . \;
#	find . -type f -name *.db  -exec rm -rf {} \;
#	find . -type f -name *.png  -exec cp {} . \;
#	find . -type f -name *.PNG  -exec cp {} . \;
#	find . -type f -name *.gif  -exec cp {} . \;
#	find . -type f -name *.GIF  -exec cp {} . \;
#
# #	CONVERTIR LAS IMÁGENES A MENOS CALIDAD
#
#	chmod 777 *.*
#	minusculas.sh
#
#	echo "RECOMPRIMIENDO IMÁGENES..."
#	for b in *.jpg *.png *.gif
#	do
#		convert -quality $QUALITY "$b" "$b".jpg
#		rm "$b"
#		echo -n .
#	done
#
#
# #	SALIMOS DE COMIC Y COMPRIMIMOS EN ZIP
#	cd ..
#
#	echo "CERRRANDO $i"
#
#	zip "$out" comic/*
#	rm -rf comic
#
#
#done


