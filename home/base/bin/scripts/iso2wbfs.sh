#!/bin/bash

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



for i in *.iso
do

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""wbfs"

	echo "========================================"
	echo -n "CONVIRTIENDO: $i"
	wit COPY "$i" "$vidout"
	echo " ok!"


done
