#!/bin/bash

QUALITY=75

mkdir bak


for i in *.rar
do

	echo "ABRIENDO $i"
	mkdir comic

# 	DESCOMPRIMIR EN COMIC
	unrar x "$i" comic

# 	ENTRAR EN COMIC
	cd comic
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS

	find . -type f -name *.jpg  -exec cp {} . \;
	find . -type f -name *.jpeg  -exec cp {} . \;
	find . -type f -name *.JPG  -exec cp {} . \;
	find . -type f -name *.JPEG  -exec cp {} . \;
	find . -type f -name *.db  -exec rm -rf {} \;
	find . -type f -name *.png  -exec cp {} . \;
	find . -type f -name *.PNG  -exec cp {} . \;
	find . -type f -name *.gif  -exec cp {} . \;
	find . -type f -name *.GIF  -exec cp {} . \;

# 	CONVERTIR LAS IMÁGENES A MENOS CALIDAD

	echo "RECOMPRIMIENDO IMÁGENES..."
	for b in *.jpg *.png *.gif
	do
		convert -quality $QUALITY "$b" "$b".png
		rm "$b"
		echo -n .
	done

	optipng *.png

# 	SALIMOS DE COMIC Y COMPRIMIMOS EN RAR
	cd ..

	echo "CERRRANDO $i"

	zip "$i".cbz comic/*
#	rar a "$i".cbr comic/
	rm -rf comic

done


# 	HACEMOS LO MISMO PARA LOS ZIP

for i in *.zip
do

	echo "ABRIENDO $i"
	mkdir comic

# 	DESCOMPRIMIR EN COMIC
	unzip "$i" -d comic

# 	ENTRAR EN COMIC
	cd comic
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS

	find . -type f -name *.jpg  -exec cp {} . \;
	find . -type f -name *.jpeg  -exec cp {} . \;
	find . -type f -name *.JPG  -exec cp {} . \;
	find . -type f -name *.JPEG  -exec cp {} . \;
	find . -type f -name *.db  -exec rm -rf {} \;
	find . -type f -name *.png  -exec cp {} . \;
	find . -type f -name *.PNG  -exec cp {} . \;
	find . -type f -name *.gif  -exec cp {} . \;
	find . -type f -name *.GIF  -exec cp {} . \;

# 	CONVERTIR LAS IMÁGENES A MENOS CALIDAD

	echo "RECOMPRIMIENDO IMÁGENES..."
	for b in *.jpg *.png *.gif
	do
		convert -quality $QUALITY "$b" "$b".png
		rm "$b"
		echo -n .
	done

	optipng *.png


# 	SALIMOS DE COMIC Y COMPRIMIMOS EN RAR
	cd ..

	echo "CERRRANDO $i"

	zip "$i".cbz comic/*
#	rar a "$i".cbr comic/
	rm -rf comic
done

mv *.zip bak
mv *.rar bak
minusculas.sh
minusculas.sh
