#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
# vidbitrate=6000

for i in *.mp4
do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""x264.mkv"


#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -vpre slow_firstpass -b 900k -bt 900k -threads 0 "$vidout"

#ffmpeg -i "$origen" -acodec libvorbis -ar 44100 -ab 64k -ac 2 -pass 2 -vcodec libx264 -vpre slow -b 900k -bt 900k -threads 0  -y "$vidout"

# MÉTODO POR PARTES PARA EVITAR CORTES EN SONIDO:

#ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 "$vidout".mp3
#pacpl -t ogg "$vidout".mp3 -o mp3  --bitrate 68 --oggqual 0 --outdir .
#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -vpre slow_firstpass -b 900k -bt 900k -threads 0 "$vidout".avi
#ffmpeg -i "$origen" -an -pass 2 -vcodec libx264 -acodec -vpre slow -b 900k -bt 900k -threads 0  -y "$vidout".avi
#mencoder "$vidout".tmp.mkv -ovc copy -oac vorbis -audiofile "$vidout".mp3 -o "$vidoutput"
#ffmpeg -i "$origen" -acodec -acodec libvorbis -ar 44100 -ab 64k -ac 2  kk.x264.mkv
#ffmpeg  -i "$vidout".avi  -i "$vidout".ogg  -acodec copy -vcodec copy  kk.x264.mkv



#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -vpre slow_firstpass -b 900k -bt 900k -threads 0 "$vidout"

#ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libx264 -vpre slow -b 900k -bt 900k -threads 0  -y "$vidout"


#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264  -b 900k -bt 900k -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 "$vidout"

ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec copy -threads 0  -y "$vidout"

rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree



done 

