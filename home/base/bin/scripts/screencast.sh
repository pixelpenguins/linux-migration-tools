#!/bin/sh
# SCREENCAST BY JESUSDA
# SISTEMA DE GRABACIÓN DE SCREENCAST CON SOX, FFMPEG Y MENCODER.


fname=`date +"screencast_%Y-%m-%d_%m-%M_%S"`
begindate=`date +"%s"`

formatovideo=avi


clear

echo "===================================================="
echo 
echo -n "ACTIVANDO GRABACIÓN DE AUDIO... "

# -q oculta la salida de SOX. Para debug, quitar este parámetro.
sox -q -t alsa "hw:1,0"  -t ogg -c1 audio.ogg rate 44100 gain +16 dither -s > /dev/null &
soxpid=`ps -A | grep sox | awk '{print $1}'`

echo "Ok!"
echo
echo "===================================================="
echo 
echo "ACTIVANDO GRABACIÓN DE VÍDEO... Ok!"
echo 
echo "Pulsa la tecla Q para terminar la grabación"
echo
sleep 1

# -loglevel panic oculta la salida de ffmpeg. Para debug, quitar este parámetro.

#/opt/ffmpeg/bin/ffmpeg -f x11grab -r 25 -s 1024x768 -r 25 -i :0.0 -sameq video."$formatovideo"
ffmpeg -loglevel panic -f x11grab -r 12 -s 2832x1050 -r 12 -i :0.0 -qscale 0 video."$formatovideo"


kill -9 $soxpid 2> /dev/null

echo "===================================================="
echo 
echo "UNIENDO AUDIO Y VÍDEO..."
echo 

# primero cortaremos el primer segundo del audio, ya que como el scrtip comienza a grabar el audio antes que el vídeo, hay siempre un desfase de un segundo en el que el audio va adelantado.

ffmpeg -y -ss 00:00:01 -i audio.ogg -acodec copy audio_cut.ogg


mencoder video."$formatovideo" -ovc copy -oac mp3lame -audiofile audio_cut.ogg -o "$fname".mkv

mv audio_cut.ogg "$fname".bak.ogg
mv video."$formatovideo" "$fname".bak."$formatovideo"

rm audio.ogg -rf
#rm audio.ogg video."$formatovideo"



enddate=`date +"%s"`
timediff=`expr $enddate - $begindate`
mins=`expr $timediff / 60`
secs=`expr $timediff % 60`

clear 
echo "===================================================="
echo 
echo "La grabación $fname duró $mins minutos y $secs segundos"
echo 
echo "===================================================="


exit




