#!/bin/bash


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}

function failure {

	echo -n "."

	mv "$namefich"bak "$namefich"pdf 
	echo -n " FAIL! ..."

	
}

function sizecompare {

	# Si el archivo generado es de mayor o igual tamaño que el
	# original, entonces nos quedamos con el original.

	echo -n "."

	sizeold=`du -b "$namefich"bak  | cut -f1`
	sizenew=`du -b "$namefich"pdf  | cut -f1`

	if [  $sizeold -le $sizenew ];
	then
		mv "$namefich"bak "$namefich"pdf 
		echo " FAIL!"

	else

		echo " OK!"

	fi

	
}



echo "REQUIERE TENER INSTALADOS pdftk"


for i in *.pdf
do

	echo -n "Procesando $i" 

	cambiaext "$i"
	echo -n "."

	mv "$i" "$namefich"bak
	echo -n "."

	pdftk "$namefich"bak output "$namefich"pdf compress 2> /dev/null || failure 
	sizecompare


	



done

