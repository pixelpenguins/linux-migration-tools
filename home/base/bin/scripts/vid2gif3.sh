#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}




	origen="$1"
	cambiaext "$1"
	vidout=$namefich"gif"

	ffmpeg -i "$origen" vidout.gif


	convert -delay 1x20 -loop 0 out*.gif animation.gif 


# Tiempo
# ffmpeg -t 5 -ss 00:00:10 -i VIDEO SALIDA.gif

# Escalado
# ffmpeg -i atop.ogv -vf scale=800:-1:flags=lanczos atop.gif




