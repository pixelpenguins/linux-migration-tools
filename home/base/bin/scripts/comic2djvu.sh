#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



QUALITY=75
tecla=a



echo "PREPARANDO..."






for i in *.cbr
do


	origen="$i"
	cambiaext "$i"
	out=$namefich"tmp.djvu"
	out2=$namefich"djvu"

	echo "ABRIENDO $i"
	mkdir comic 2> /dev/null
	
# 	DESCOMPRIMIR EN COMIC
	echo "DESCOMPRIMIENDO $i"
	unrar x -inul "$i" comic 2> /dev/null


# 	ENTRAR EN COMIC
	echo "ENTRANDO EN CÓMIC..."
	cd comic
	#ls
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS
	echo "AGRUPANDO IMÁGENES..."

	mkdir imagespdf 

	find . -type f -iname "*.jpg"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.jpeg"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.JPG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.JPEG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.png"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.PNG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.gif"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.GIF"  -exec cp {} ./imagespdf \; 2> /dev/null




	echo "CREANDO DJVUS FILES..."

	cd imagespdf 

	for i in *.png
	do
		
		mogrify $i "$i".png

	done

	for i in *.jpg
	do
		
		c44 $i

	done


	

	#cd ../..

	echo -n "UNIENDO DJVUS..."

	djvm -c ../../"$out2" *.djvu

	echo "TERMINANDO... $i"

	rm -rf *_temp.djvu

	cd ../..


done


for i in *.cbz
do


	origen="$i"
	cambiaext "$i"
	out=$namefich_temp"djvu"
	out2=$namefich_final"djvu"

	echo "ABRIENDO $i"
	mkdir comic 2> /dev/null
	
# 	DESCOMPRIMIR EN COMIC
	echo "DESCOMPRIMIENDO $i"
	unzip -qq "$i" -d comic 2> /dev/null


# 	ENTRAR EN COMIC
	echo "ENTRANDO EN CÓMIC..."
	cd comic
	#ls
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS
	echo "AGRUPANDO IMÁGENES..."

	mkdir imagespdf 

	find . -type f -iname "*.jpg"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.jpeg"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.JPG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.JPEG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.png"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.PNG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.gif"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.GIF"  -exec cp {} ./imagespdf \; 2> /dev/null




	echo "CREANDO DJVUS FILES..."

	cd imagespdf 

	echo
	echo -n "Convirtiendo a JPG..."

	for i in *.png
	do
		echo -n "."
		convert $i "$i".jpg

	done

	echo
	echo -n "Convirtiendo a DJVU..."

	for i in *.jpg
	do
		echo -n "."
		c44 $i

	done


	

	#cd ../..

	echo 
	echo -n "UNIENDO DJVUS..."

	djvm -c ../../"$out2" *.djvu

	echo "TERMINANDO... $i"

	cd ../..

	rm -rf comic



done


echo "TERMINADO... ;)"

