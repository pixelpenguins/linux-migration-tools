#!/bin/bash
# Convierte archivos markdown a pdf
#
# requiere los paquetes:
# pandoc texlive-fonts-recommended texlive-xetex

pandoc --pdf-engine=xelatex --variable mainfont="DejaVu Serif" --variable sansfont="DejaVu Sans" -V papersize:a4 -V geometry:margin=0.8in "$1" -o "$1".pdf
