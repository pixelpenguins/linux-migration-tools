#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}






for i in *.mp4 *.mov

do 

	origen="$i"
	cambiaext "$i"
	vidtemp="$namefich""tmp.mp4"
	vidout="$namefich""x264.mp4"
	bitrate="815k"


echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k

echo "el bitrate optimo es $bitrateoptimo"
echo "codificando a $bitrateoptimo"


bitrateoptimo=$bitrate



#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -r 30 -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -s 640x360 -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 "$vidout"

#ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libx264 -r 30 -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -s 640x360 -vf unsharp=5:5:1.0:5:5:1.0 -threads 0  -y "$vidout"


ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -r 25 -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -s 854x480 -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 "$vidout"

ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 96k -ac 1 -pass 2 -vcodec libx264 -r 25 -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -s 854x480 -vf unsharp=5:5:1.0:5:5:1.0 -threads 0  -y "$vidout"

rm -rf "$vidtemp"
rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree

done 




