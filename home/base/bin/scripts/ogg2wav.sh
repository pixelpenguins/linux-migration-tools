#!/usr/bin/perl

# ogg2wav, a simple script to create wav files throug ogg ones.
# Generated WAV files have big size (+- 10 MB per min or more).

# Original Script Author: unknown (notify me if you know where and who is him).
# Atuthor: jEsuSdA <jesusda@ono.com>
# This software released under the terms of the GPL LICENSE.
# <http://www.opensource.org/licenses/gpl-license.php>

# version 1.2.0 by jEsuSdA.


$dir=`pwd`;
$version = "v1.2.0";

print "ogg2wav $version\n";
print "(c) 2002-2005 jEsuSdA\n";
print "Released without warranty under the terms of the GPL License\n\n";

## READ EACH FILE, IF OGG, MAKE A "COPY" TO WAV.

chop($dir);

opendir(checkdir,"$dir");

while ($file=readdir(checkdir)) {

$orig_file=$file;

if ($orig_file !~ /\.ogg$/i) {next};

print "Checking file: $orig_file\n";


$new_wav_file=$orig_file;$new_wav_file=~s/\.ogg/\.wav/;

$convert_to_wav="mplayer \"./$orig_file\" -ao pcm:file=\"./$new_wav_file\"";


print "EXEC 1: $convert_to_wav\n";
$cmd=`$convert_to_wav`;
print "\n\n";

}

print "Done.";


