#!/bin/bash

mkdir bak

for i in *.gif
do

	origen="$i"
	cp "$i" bak/
	gifsicle -O3 --colors 200 "$i" > "$i"_new
	mv "$i"_new "$i"


done

exit

