#!/bin/bash

QUALITY=75
SIZE=50%


for a in *.jpeg
do
	b=`echo "$a" | sed 's/jpeg/jpg/g'`
	# usamos sed 's/*/*/g' donde el primer asterisco se sustituye por el segundo.
	# | sed 's///g' 
	mv "$a" "$b"
done



#for i in *.jpg *.jpeg *.tbn
for i in *.jpg
do

	origen="$i"
#	nice -9 mogrify -resize $SIZE -quality $QUALITY "$origen" 
 	nice -9 mogrify -quality $QUALITY -type optimize "$origen"
	echo -n .
done

exit

