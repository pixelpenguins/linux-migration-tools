#!/bin/sh

GNASH="/usr/bin/gtk-gnash"
SCROT="/usr/bin/scrot"
OUTPUTFORMAT="jpg"

for i in *.swf 
do

	$GNASH --fullscreen "$i" &
  		GNASHPID=$!
  		$SCROT -d 1 "$i".$OUTPUTFORMAT
  	kill $GNASHPID
done