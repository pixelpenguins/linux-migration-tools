#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}






for i in *.mkv

do 

	origen="$i"
	cambiaext "$i"
	vidtemp="$namefich""tmp.mkv"
	vidout="$namefich""vp9.mkv"
	bitrate="915k"


echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k

echo "el bitrate optimo es $bitrateoptimo"
echo "codificando a $bitrateoptimo"



# QUITAMOS LOS SUBTÍTULOS

mkvmerge --no-subtitles "$origen" -o "$vidtemp"


ffmpeg -i "$vidtemp" -an -pass 1 -vcodec libvpx-vp9 -strict experimental -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -s 854x480 -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 "$vidout"

ffmpeg -i "$vidtemp" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libvpx-vp9 -strict experimental -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -s 854x480 -vf unsharp=5:5:1.0:5:5:1.0 -threads 0  -y "$vidout"

rm -rf "$vidtemp"
rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree

done 









for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv  *.dv *.vob *.m4v *.webm

do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""vp9.mkv"


echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k

echo "el bitrate optimo es $bitrateoptimo"
echo "codificando a $bitrateoptimo"


#ffmpeg -y -i input.mkv -c:v libvpx-vp9 -b:v 2000k -pass 1 -an -f webm /dev/null
#ffmpeg    -i input.mkv -c:v libvpx-vp9 -b:v 2000k -pass 2 -c:a opus -b:a 64k -f webm output.webm

ffmpeg -y -i "$origen" -c:v libvpx-vp9 -b:v "$bitrateoptimo" -pass 1 -an -s 854x480 -vf unsharp=5:5:1.0:5:5:1.0  -threads 0 "$vidout"

ffmpeg -i "$origen" -c:v libvpx-vp9 -b:v "$bitrateoptimo" -pass 2 -s 854x480 -vf unsharp=5:5:1.0:5:5:1.0 -c:a opus -b:a 64k -threads 0 -y "$vidout"



rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree

done 



