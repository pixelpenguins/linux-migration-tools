#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
# vidbitrate=6000

for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.ogg *.ogm  *.dv
do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""x264.mkv"

#	mv "$origen" "$origen"".bak"

# 	mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts subq=5:8x8dct:frameref=2:bframes=3:b_pyramid=normal:weight_b -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=96:q=7:aq=9:cbr:mode=1

# 	mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts subq=6:partitions=all:8x8dct:me=umh:frameref=5:bframes=3:b_pyramid=normal:weight_b -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=96:q=7:aq=9:cbr:mode=1


# 	mencoder "$origen" -o /dev/null -ss 32 -ovc x264 -x264encopts pass=1:turbo:subq=6:partitions=all:8x8dct:me=umh:frameref=5:bframes=3:b_pyramid=normal:weight_b:threads=auto -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=96:q=7:aq=9:cbr:mode=1



	mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts subq=6:frameref=5:partitions=all:8x8dct:me=umh:frameref=5:bframes=3:b_pyramid=normal:weight_b:threads=auto -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=96:q=7:aq=9:cbr:mode=1



	#Very high quality	
	#subq=6:partitions=all:8x8dct:me=umh:frameref=5:bframes=3:b_pyramid=normal:weight_b

	#High quality (45fps)
	#subq=5:8x8dct:frameref=2:bframes=3:b_pyramid=normal:weight_b

	#Fast
	#subq=4:bframes=2:b_pyramid=normal:weight_b


#mencoder "$origen" -o "$vidout" -ovc xvid -xvidencopts bitrate=1000 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1






done 

