#!/bin/bash


# GENERA UN VIDEO A PARTIR DE UNA IMAGEN

# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


mkdir bak

for i in *.*
do

	origen="$i"
	cambiaext "$i"
	out=$namefich"png"

cp "$origen" bak/

convert -format png -auto-level -normalize -modulate 105,00,00 -colorspace gray -colors 9 -auto-level  -normalize "$i" "$out"

echo -n "."

done

echo "OK!"



