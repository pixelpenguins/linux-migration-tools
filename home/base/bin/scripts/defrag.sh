#!/bin/bash
# Copyright (C) 2010 Timothy McGrath <tmhikaru@gmail.com>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# But WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# defrag v0.11 by Timothy McGrath <tmhikaru@gmail.com>

# defrag v0.08 by Con Kolivas <kernel@kolivas.org>
# defrag v0.08 and all earlier versions were created by Con Kolivas. I could
# not have invented such a useful script myself, I have merely improved upon
# the work of another.
# Please do not contact Con Kolivas for support with this program.

if [ "$1" ]; then
	for arg in "$@"; do
		if [ "$arg" == "--safe" ] || [ "$arg" == "-s" ]; then
			safe=TRUE
		fi
		if [ "$arg" == "--reverse-sort" ] || [ "$arg" == "-r" ]; then
			reverse=TRUE
		fi
		if [ "$arg" == "--no-filefrag" ]; then
			nofilefrag=TRUE
		fi
		if [ "$arg" == "--fragsort" ] || [ "$arg" == "-f" ]; then
			fragsort=TRUE
		fi
		if [ "$arg" == "--defrag" ] || [ "$arg" == "-d" ]; then
			defrag=TRUE
		fi
		if [ "$arg" ] && [ "$safe" != "TRUE" ] && [ "$reverse" != "TRUE" ] \
		&& [ "$nofilefrag" != "TRUE" ] && [ "$fragsort" != "TRUE" ] && [ "$defrag" != "TRUE" ]; then
			# Default, so people doing --help/-h get some output
			echo "defrag v0.11 by Timothy McGrath <tmhikaru@gmail.com>"
			echo "defrag v0.08 and earlier by Con Kolivas <kernel@kolivas.org>"
			echo "Please do not contact Con Kolivas for support with this program."
			echo
			echo "Braindead fs-agnostic defrag to rewrite files. Run this in the directory you"
			echo "want all the files and subdirectories to be reordered. It will only affect one"
			echo "partition. It works best when run twice."
			echo "Are you really crazy enough to be using this? It might blow your data into tiny"
			echo "little useless chunks."
			echo
			echo "Options:"
			echo "-s, --safe          Check for open files. Much slower, but safer"
			echo "-r, --reverse-sort  Invert sort, largest directories and files are processed"
			echo "                    first."
			echo "-f, --fragsort      Sort by most fragmented files first. This tends to unwedge"
			echo "                    defragmenting when larger files are very badly fragmented."
			echo "--no-filefrag       Use this option if know your filesystem is not supported by"
			echo "                    the filefrag program and defrag is guessing this wrong"
			echo "-d, --defrag        Actually try to defragment the filesystem rather than just"
			echo "                    report how fragmented things are."
			exit 0
		fi
	done
fi

trap 'abort' 1 2 15

renice 19 $$ > /dev/null

abort()
{
	echo -e "\nAborting"
	rm -f tmpfile dirlist totalfrags
	exit 1
}

fail()
{
	echo -e "\nFailed"
	abort
}

declare -i filesize=0
declare -i numfiles=0
declare -i oldfrags=0
declare -i newfrags=0
declare -i oldfragstotal=0
declare -i newfragstotal=0

#The maximum size of a file we can easily cache in ram
declare -i maxsize=$((`awk '/MemTotal/ {print $2}' /proc/meminfo`*1024))
(( maxsize-= `awk '/Mapped/ {print $2}' /proc/meminfo` ))
(( maxsize/= 2))

if [[ -a fragstats ]] ; then
	echo fragstats exists. Move it out of the way or delete it
	exit 1
fi

if [[ -a tmpfile || -a dirlist || -a totalfrags ]] ; then
	echo dirlist, tmpfile or totalfrags exists
	exit 1
fi


# Find filefrag, then test if it is functional (functional period, or this
# filesystem, doesn't matter. If it doesn't work it doesn't work)

if [ "$nofilefrag" != "TRUE" ]; then
	#Lets try to find filefrag in some likely places
	#Was it set by the user on the command line?
	if [ "$filefrag" == "" ]; then
		#Nope. Is it on the path?
		which filefrag &> /dev/null
		if [ $? -eq 0 ]; then
       			filefrag="filefrag"
		fi
	fi	
	if [ "$filefrag" == "" ]; then
		#Not on the path. Is it in /sbin?
		which /sbin/filefrag &> /dev/null
		if [ $? -eq 0 ]; then
       			filefrag="/sbin/filefrag"
		else
			#Stumped!
			echo "Can't find filefrag, do you have e2fsprogs installed?"
			echo "Run with filefrag=/path/to/filefrag $0"
			echo "if it is installed but I can't find it."
			echo
			echo "If it is not installed you can use the --no-filefrag"
			echo "switch, but this will make defragmentation much less"
			echo "efficient and is not recommended."
			abort
		fi
	fi
	#Found an executable, but does it work?
	echo "1" > tmpfile
	fragtest=`"$filefrag" -s "tmpfile" | grep " found" | sed -e 's/.*: //' | sed -e 's/ extent.*//'`
	if [ "$fragtest" == "" ]; then
		#filefrag is broken or otherwise giving us useless output. DISABLED.
		nofilefrag=TRUE
	fi
	rm -f tmpfile
fi

if [ "$defrag" != "TRUE" ]; then
	if [ "$nofilefrag" == "TRUE" ]; then
		echo "Not able to report fragmentation without filefrag available. Aborting!"
		abort
	fi
	echo "Defragmentation is disabled. Will only report fragmentation of files."
else
	echo "Defragmentation is enabled"
fi

if [ "$fragsort" == "TRUE" ]; then
	if [ "$nofilefrag" == "TRUE" ]; then
		echo "Cannot sort by frags, filefrag is unusable on this filesystem or otherwise disabled!"
		abort
	fi
	echo -e "Checking ten most fragmented files"
else
	if [ "$reverse" != "TRUE" ]; then
		echo -e "Files sorted by size ascending"
	else
		echo -e "Files sorted by size descending"
	fi
fi

if [ "$safe" != "TRUE" ]; then
	echo -e "Won't skip open files (Faster, but less safe)"
else
	echo -e "Will skip open files (Slower, but safer)"
fi

if [ "$nofilefrag" == "TRUE" ]; then
	echo "Fragmentation checking via filefrag disabled by user or is otherwise nonfunctional on"
	echo "this filesystem. Defrag will still attempt to perform work, but will be less efficient"
fi

echo "Creating list of files..."


if [ "$fragsort" != "TRUE" ]; then
	#sort files in descending/ascending size order only

	if [ "$reverse" != "TRUE" ]; then
		find -xdev \! -name totalfrags \! -name fragstats \! -name dirlist \! -links +1 -type f -printf "%s\t%p\n" | \
		sort -k 1,1n | \
		cut -f2 >> dirlist
	else
		find -xdev \! -name totalfrags \! -name fragstats \! -name dirlist \! -links +1 -type f -printf "%s\t%p\n" | \
		sort -k 1,1nr | \
		cut -f2 >> dirlist
	fi
	if (( $? )) ; then
		fail
	fi
                                                                                                                                                                
else
	#sort files in descending by frags only, limit the list to the first ten files
	find -xdev \! -name totalfrags \! -name fragstats \! -name dirlist \! -links +1 -type f -exec "$filefrag" -s "{}" \; | \
	sed -n '/found/s/\(^.*\): \([0-9]*\)[^:]*$/\2 \1/p' | \
	sort -nr | \
	sed -e 's/^[0-9]* //' | \
	head >> dirlist

	if (( $? )) ; then
		fail
	fi
fi

numfiles=`wc -l dirlist | awk '{print $1}'`
echo -e "$numfiles files will be checked\n"

#copy to temp file, check the file hasn't changed and then overwrite original
cat dirlist | while read i;
do
	(( --numfiles ))

	if [[ ! -f "$i" ]]; then
		echo -e "\r$numfiles files left. Current MISSING, -- SKIPPED                               \c"
		continue
	fi

	# This is safer, but much slower.
	if [ "$safe" == "TRUE" ] && [ "$defrag" == "TRUE" ]; then
		if [[ `lsof -f -- "$i"` ]]; then
			echo -e "\r$numfiles files left. Current OPEN, -- SKIPPED                                  \c"
			continue
		fi
	fi

	filesize=`find "$i" -printf "%s"`
	if [ "$nofilefrag" != TRUE ]; then
		oldfrags=`"$filefrag" -s "$i" | grep " found" | sed -e 's/.*: //' | sed -e 's/ extent.*//'`

		if [ "$oldfrags" -le 1 ]; then #A file with no contents can have 0 extents!
			#What are we bothering with a perfect file for?
			oldfragstotal+="$oldfrags"
			newfragstotal+="$oldfrags"
			echo "$oldfragstotal $newfragstotal" > totalfrags
			echo -e "\r$numfiles files left. Current NOT FRAGMENTED, -- SKIPPED                        \c"
			continue
		elif [ "$defrag" != "TRUE" ]; then
			#We're not defragmenting
			oldfragstotal+="$oldfrags"
			newfragstotal+="$oldfrags"
			echo "$oldfragstotal $newfragstotal" > totalfrags
			echo -e "\r$numfiles files left. Defragmentation disabled.                                 \c"
			continue
		fi

		oldfragstotal+="$oldfrags"
		echo "$oldfragstotal $newfragstotal" > totalfrags
	fi

	# read the file first to cache it in ram if possible
	if (( filesize < maxsize )); then
		# Cut down the filename&path to fit on an 80 column screen
		pathlen=80 #80 columns
		firstpart="$numfiles files left. Current "
		len="${#firstpart}"
		pathlen=$((pathlen - len))
		if [ "${#i}" -gt "${pathlen}" ] ; then
			offset=$[${#i} - ${pathlen}]
		else
			offset=0
		fi
		secondpart=${i:${offset}:${#i}}
		echo -e "\r$firstpart$secondpart\c"
		cat "$i" > /dev/null
	else
		# Cut down the filename&path to fit on an 80 column screen
		pathlen=80 #80 columns
		firstpart="$numfiles files left. Current sized $filesize is "
		len="${#firstpart}"
		pathlen=$((pathlen - len))
		if [ "${#i}" -gt "${pathlen}" ] ; then
			offset=$[${#i} - ${pathlen}]
		else
			offset=0
		fi
		secondpart=${i:${offset}:${#i}}
		echo -e "\r$firstpart$secondpart\c"
	fi

	datestamp=`find "$i" -printf "%s"`
	cp -a -f "$i" tmpfile
	if (( $? )) ; then
		fail
	fi
	
	# Clear the previous line's data
	echo -e "\r                                                                                \c"

	if [ "$nofilefrag" != "TRUE" ]; then
		newfrags=`"$filefrag" -s "tmpfile" | grep " found" | sed -e 's/.*: //' | sed -e 's/ extent.*//'`
		if [ "$oldfrags" -lt "$newfrags" ]; then
			echo "$i got WORSE! old $oldfrags new $newfrags - skipped" >> fragstats
			newfragstotal+="$oldfrags"
			echo "$oldfragstotal $newfragstotal" > totalfrags
			rm -f "tmpfile"
			continue
		fi
		newfragstotal+="$newfrags"
		echo "$oldfragstotal $newfragstotal" > totalfrags
	fi

	# check the file hasn't been altered since we copied it
	if [[ `find "$i" -printf "%s"` != "$datestamp" ]] ; then
		rm -f "tmpfile"
		continue
	fi

	mv -f tmpfile "$i"
	if (( $? )) ; then
		fail
	fi

	if [ "$nofilefrag" != "TRUE" ]; then
		if [ "$oldfrags" -gt "$newfrags" ]; then
			echo "$i improved old $oldfrags new $newfrags" >> fragstats
		elif [ "$oldfrags" -eq "$newfrags" ]; then
			echo "$i is the same" >> fragstats
		fi
	fi
done

if [ "$nofilefrag" != "TRUE" ] && [ "$defrag" == "TRUE" ]; then
	oldfragstotal=`cat totalfrags | cut -d" " -f1`
	newfragstotal=`cat totalfrags | cut -d" " -f2`
	rm -f totalfrags

	totalpercentage=`echo "scale=2; $newfragstotal / $oldfragstotal * 100" | bc`
	avgbeforefile=`echo "scale=2; $oldfragstotal / $numfiles" | bc`
	avgafterfile=`echo "scale=2; $newfragstotal / $numfiles" | bc`

	echo -e "\nSucceeded, files now $totalpercentage% as fragmented as before"
	echo    "Before we had $oldfragstotal extents"
	echo    "After we have $newfragstotal extents"
	echo	"For an average before of $avgbeforefile extents per file"
	echo    "And now an average of $avgafterfile extents per file"
	echo    "The closer to a ratio of 1, the better."
elif [ "$defrag" != "TRUE" ]; then
	oldfragstotal=`cat totalfrags | cut -d" " -f1`
	avgbeforefile=`echo "scale=2; $oldfragstotal / $numfiles" | bc`
	rm -f totalfrags

	echo -e "\nFinished checking fragmentation of files."
	echo    "Found that the files have a total of $oldfragstotal extents"
	echo    "for an average of $avgbeforefile extents per file."
	echo    "The closer to a ratio of 1, the better."

else
	echo -e "\nSucceeded, but could not generate statistics because filefrag is"
	echo    "nonfunctional or you used --no-filefrag"
fi

rm -f dirlist
