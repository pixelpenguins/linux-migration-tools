#!/bin/bash
# Requiere tener instalado el paquete mkvtoolnix.


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



# **************************************************************************


for i in *.mkv
do 

	origen="$i"
	cambiaext "$i"
	vidout=$namefich"avi"

	#EXTRAYENDO VIDEO, AUDIO Y SUBTÍTULOS
#	mkvextract tracks "$origen" 1:temp_video.avi 2:temp_audio.ogg 3:"$namefich""srt" 
	mkvextract tracks "$origen" 3:"$namefich""srt" 

#	ogg2wav temp_audio.ogg temp_audio.wav

	# MEZCLANDO TODO


	mencoder "$origen" -o "$vidout" -ovc xvid -xvidencopts bitrate=1700 -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=96:q=7:aq=9:cbr:mode=1


#	ffmpeg -i temp_audio.ogg  -i temp_video.avi  -vcodec copy  "$namefich""avi"

#	mencoder "temp_video.avi" -o "$namefich""avi" -ovc lavc lavcopts vcodec=xvid:mbd=2:vhq:vbitrate=8 -ofps 30,000000 -sub "$namefich""srt" -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1

#	mencoder "temp_video.avi" -o "$namefich""avi" -ovc xvid -xvidencopts bitrate=1500 -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -ofps 30,000000 -sub "$namefich""srt" -audiofile temp_audio.ogg -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1

#	mencoder "temp_video.avi" -o "$namefich""avi" -ovc xvid -xvidencopts bitrate=1500 -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -ofps 30,000000 -sub "$namefich""srt" -audiofile temp_audio.ogg -srate 44100 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1


	# BORRAR TEMPORALES:
	rm temp_* -rf


done 


# **************************************************************************






#import sys
#import os

#def message(msg):
#    print "=" * 78
#    print "= %s" % msg
#    print "=" * 78

#def usage():
#    print "Mastroka repacker script"
#    print "  Usage: "+sys.argv[0]+ " filename"

#if __name__ == "__main__":
#    if len(sys.argv) < 2:
#        usage()
#    else:
#        filename = sys.argv[1]
#        basename = filename[:-4]

#        message("Unpacking file: %s" % filename)
#        os.system("mkvextract tracks %s 1:temp_video.avi 2:temp_audio.ogg 3:%s.srt" % (filename,basename) )

#        message("Repacking file: %s.avi" % basename)
#        os.system("ffmpeg -i temp_audio.ogg  -i temp_video.avi  -vcodec copy  %s.avi" % (basename) )

#        message("Cleaning files")
#        os.system("rm temp_video.avi temp_audio.ogg")


