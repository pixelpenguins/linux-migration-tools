#!/bin/bash

echo "Desea escalar todas las imágenes para optimizarlas para web?"
echo "Ctrl+C si NO lo desea, cualquier tecla para proceder..."
read key

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# ======================================
# BACKUP
# ======================================
mkdir img-bak

minusculas.sh
minusculas.sh
minusculas.sh

# ======================================
# QUITAMOS ESPACIOS
# ======================================

rename -f -v 's/ /-/' *.jpg
rename -f -v 's/ /-/' *.png



# ======================================
# CAMBIAMOS JPEG A JPG
# ======================================
for a in *.jpeg
do
 	origen1="$a"
 	cambiaext "$a"
 	out1=$namefich
 	mv "$a" "$out1"jpg

done

# ======================================
# CONVERTIMOS GIF's A JPG's
# ======================================
for gif in *.gif
do

echo "conviertiendo gifs -> $b"
#read $key
	if [ -z "$gif" ]
	then
		echo "No hay gif's."
	elif [ "$gif" = "*.gif" ]
	then
		echo "No hay gif's."
	else
	 	origengif="$gif"
	 	cambiaext "$gif"
	 	outgif=$namefich
	 	convert -quality 100 "$gif" "$outgif"jpg
	 	rm -rf "$gif"
	fi
done



# ======================================
# CONVERTIMOS PNG's A JPG's
# ======================================
for png in *.png
do
echo "conviertiendo pngs -> $png"
#read $key
	if [ -z "$png" ]
	then
		echo "No hay png's."
	elif [ "$png" = "*.png" ]
	then
		echo "No hay png's."
	else
	 	origenpng="$png"
	 	cambiaext "$png"
 		outpng=$namefich
	 	convert -quality 100 "$png" "$outpng"jpg
	 	rm -rf "$png"
	fi
done




# ======================================
# OPTIMIZAMOS LAS IMÁGENS
# ======================================

for imagen in *.jpg
do

	echo "optimizando -> $imagen"
	# read $key

		origen3="$imagen"
		cambiaext "$imagen"
		out3=$namefich


	 echo "Procesando $imagen ..."
	# read $key


	echo -n "    creando copia de seguridad..."
		cp "$origen3" img-bak
		chmod +w "$origen3"
		echo " Ok!"


	echo -n "    procesando tamaño y calidad..."


	NEWNAME=`basename "$origen3".png`
	echo -e "Sharpening "$origen3" into $NEWNAME\n\nEdge detect:"
convert -monitor -edge 2 "$origen3" orig_edge.png
	echo -e "\nUnsharp original:"
convert -monitor -unsharp 1x1+1+.01 "$origen3" temp_sharp.png
	echo -e "\nSoftening edge:"
convert -monitor -threshold 40% -blur 2 -threshold 30% -blur 2 orig_edge.png soft_edge.png
	echo -e "\nCompositing:"
composite -quality 100 -monitor temp_sharp.png "$origen3" soft_edge.png "$origen3"


	# Convertimos al tamaño final y limpiamos los metadatos
	convert "$imagen"  -resize 800x600 -gravity center -crop 800x600+0+0 -extent 800x600 +repage -strip -quality 100 "$imagen"


	# Añadimos marca de agua y optimizamos para generar el archivo definitivo
#	cp ../watermark-vm.png .
#	echo -n "    añadiendo marca de agua..."
#	composite -dissolve 100% -gravity southeast watermark-vm.png -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace RGB "$imagen" "$imagen"


	# Optimizamos para generar el archivo definitivo
	echo -n "    añadiendo marca de agua..."
	convert "$imagen" -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace RGB "$imagen"


	echo " Ok!"

done



# ======================================
# LIMPIEZA
# ======================================
rm *_edge.png
rm *_sharp.png
rm watermark-vm.png

# ======================================
# SALIDA
# ======================================
beep

