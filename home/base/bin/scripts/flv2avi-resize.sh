#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


vidscale=640:360;
vidbitrate=6000

for i in *.flv
do 

	origen="$i"
	cambiaext "$i"
	vidout=$namefich"avi"


# mencoder "$origen" -o "$vidout" -ovc lavc lavcopts vcodec=xvid:mbd=2:vhq:vbitrate=8 -ofps 30,000000 -oac mp3lame -lameopts br=96:q=7:aq=9:cbr:mode=1

#mencoder "$origen" -o "$vidout" -ovc xvid -xvidencopts bitrate=3000 -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=96:q=7:aq=9:cbr:mode=1

mencoder "$origen" -o "$vidout" -ovc xvid -xvidencopts bitrate=1700 -vf scale=640:360,unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=96:q=7:aq=9:cbr:mode=1

# ffmpeg -i "$vidout" "$vidout_2.avi"



done 

