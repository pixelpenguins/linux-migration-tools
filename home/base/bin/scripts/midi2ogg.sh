#!/bin/bash

for i in *.mid
do

	echo "Convirtiendo $i ..."
	timidity "$i" -o "$i.ogg" -Ov
	echo "$i convertido a $i.ogg OK!"
done 
