#!/bin/bash

# Este script recodifica los vídeos con x264 para que ocupen menos
# Trata de mantener la misma calidad visual (algo subjetivo)
# También añade un pequeño filtro de enfoque para mejorar la definición del vídeo
# Usa como ayuda otro script llamado bitrate-optimo.sh para elegir el bitrate del vídeo
# resultante.

# jEsuSdA 8)
# Version: 20130206


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv  *.dv *.vob *.m4v *.webm
#for i in *.avi

do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""x264.mkv"

echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.

	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k


echo "el bitrate optimo es $bitrateoptimo"
echo "codificando a $bitrateoptimo"

ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -preset slow  -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 "$vidout"

#-vf unsharp=3:3:1.0:3:3:1.0
#-vf unsharp=5:5:1.0:5:5:1.0 

# originalmente el filtro de enfoque tenía estos valores -> unsharp=5:5:1.0:5:5:1.0

ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libx264  -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf unsharp=5:5:1.0:5:5:1.0 -threads 0  -y "$vidout"

#-vf unsharp=5:5:1.0:5:5:1.0 

rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree

done 


































# -sameq  


#ffmpeg -i "$origen" -an -pass 1 -threads 0 $options "$vidout"
#ffmpeg -i "$origen" -acodec libmp3lame -ab 128k -pass 2  -threads 0 $options  -y "$vidout"




	# PARA VERSIONES MODERNAS

#mencoder "$origen" -o "$vidout" -passlogfile "$vidout.log" -nosub -vf softskip,unsharp=l5x5:0.75:c5x5:0.75,harddup -oac mp3lame -lameopts cbr:br=128:aq=4:vol=2.2:mode=2 -ovc x264 -x264encopts pass=2:bitrate=1000:nointerlaced:force_cfr:frameref=5:mixed_refs:bframes=6:b_adapt=2:b_pyramid=normal:weight_b:weightp=2:direct_pred=auto:aq_mode=1:me=umh:me_range=32:subq=8:mbtree:rc_lookahead=50:psy_rd=0.8,0.2:chroma_me:trellis=1:cabac:deblock:8x8dct:partitions=p8x8,b8x8,i8x8,i4x4:nofast_pskip:nodct_decimate:threads=auto:ssim:psnr:keyint=250:keyint_min=25


# mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts subq=5:8x8dct:frameref=2:bframes=3:b_pyramid=normal:weight_b -vf unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1 -af resample=44100 -srate 44100

# mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts  subq=6:partitions=all:8x8dct:me=umh:frameref=5:bframes=3:b_pyramid=normal:weight_b -vf unsharp=l3x3:0.75:c3x3:0.75 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1 -af resample=44100 -srate 44100






# mencoder "/mnt/cajon3/to-toast/justified/justified_1x13.avi" -o "/home/jesusda/i.avi" -nosub -vf softskip,unsharp=l5x5:0.00:c5x5:0.00,harddup -oac mp3lame -lameopts cbr:br=128:aq=4:vol=2.2:mode=1 -ovc x264 -x264encopts bitrate=1000:nointerlaced:force_cfr:frameref=4:mixed_refs:bframes=5:b_adapt=2:b_pyramid=normal:weight_b:weightp=2:direct_pred=auto:aq_mode=1:me=umh:me_range=24:subq=7:mbtree:psy_rd=0.8,0.2:chroma_me:trellis=1:cabac:deblock:8x8dct:partitions=p8x8,b8x8,i8x8,i4x4:nofast_pskip:nodct_de
# cimate:threads=auto:ssim:psnr:keyint=240:keyint_min=24


#mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts  ref=2:bframes=2:subme=6:mixed-refs=0:weightb=0:trellis=0:8x8dct=0 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1





	# PARA VERSIONES ANTIGUAS DE MENCODER

# mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts subq=5:8x8dct:frameref=2:bframes=3:b_pyramid:weight_b:bitrate=1000 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1



	# SI TODO FALLA, ENTONCES PUEDES USAR FFMPEG

# 	ffmpeg -i "$origen" -acodec libmp3lame -aq 100 -vcodec libx264 -vpre hq -crf 22 -threads 0 "$vidout"


	#Very high quality	
	#subq=6:partitions=all:8x8dct:me=umh:frameref=5:bframes=3:b_pyramid=normal:weight_b

	#High quality (45fps)
	#subq=5:8x8dct:frameref=2:bframes=3:b_pyramid=normal:weight_b

	#Fast
	#subq=4:bframes=2:b_pyramid=normal:weight_b


#mencoder "$origen" -o "$vidout" -ovc xvid -xvidencopts bitrate=1000 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1


# Two-Pass Example
# So if you wanted to encode using two-pass VBR, the command line would be something like:
# 
# ffmpeg -i INPUT -an -pass 1 -vcodec libx264 -vpre fastfirstpass -b BIT_RATE -bt BIT_RATE -threads 0 OUTPUT.mp4
# 
# Note: Use of -vpre fastfirstpass is recommended for first passes as it reduce the encoding time for the first pass while having minimal to no noticable impact on the resulting stream. Also, we don’t encode the audio in the first pass because we will not be using the data that was output.
# 
# ffmpeg -i INPUT -acodec libfaac -ab 128k -pass 2 -vcodec libx264 -vpre hq -b BIT_RATE -bt BIT_RATE -threads 0 OUTPUT.mp4
# 
# Note: We use -vpre hq in the second pass to obtain a high quality result.


# MÉTODO BÁSICO DE 2 PASADAS PARA MENCODER:
#mencoder "$origen" -o "$vidout" -ovc lavc -lavcopts vcodec=mpeg4:vpass=1 -oac copy
#mencoder "$origen" -o "$vidout" -ovc lavc -lavcopts vcodec=mpeg4:vpass=2 -oac copy





# MÉTODO 1: FUNCIONA, PERO NO SACA MUY BUENA CALIDAD Y TAMPOCO AHORRA MUCHO ESPACIO
#echo "PRIMER PASE"

#mencoder "$origen" -o "$vidout".1pass -nosub -ovc x264 -x264encopts subq=4:bframes=3:b_pyramid=normal:weight_b:pass=1:psnr:bitrate=1000 -nosound 

#echo "SEGUNDO PASE"

#mencoder "$origen" -o "$vidout"  -nosub -oac mp3lame -lameopts cbr:br=128:aq=4:vol=2.2:mode=2 -ovc x264 -x264encopts subq=6:partitions=all:8x8dct:me=umh:frameref=5:bframes=3:b_pyramid=normal:weight_b:pass=2:psnr:bitrate=1000 -oac copy 





# MÉTODO 2: IDEM QUE EL 1, UN POCO MEJOR, PERO NO MUCHO


#BITRATE=900 
#RES=640:480 


#echo "Pass 1"

#PARAMS=keyint=250:bframes=0:qp_min=10:qp_max=51:mixed_refs=0:frameref=1:deblock=0,0:qblur=0:vbv_bufsize=10000:vbv_maxrate=10000:keyint_min=25:me=hex:me_range=16:ip_factor=1.4:pb_factor=1.3:chroma_qp_offset=0:vbv_init=0.9:ratetol=1.0:cplx_blur=20:nocabac:noweight_b:b_pyramid=normal:partitions=p8x8,b8x8,i4x4:no8x8dct:nossim:deadzone_inter=21:deadzone_intra=11 
#EXTRA_PARAMS=subq=6:trellis=0: 

#mencoder "$origen" -o "$vidout" -oac mp3lame -lameopts cbr:br=128:aq=4:vol=2.2:mode=2 -ovc x264 -x264encopts bitrate=$BITRATE:$PARAMS:$EXTRA_PARAMS:pass=1  

#echo "Pass 2" 

#mencoder "$origen" -o "$vidout" -oac mp3lame -lameopts cbr:br=128:aq=4:vol=2.2:mode=2 -ovc x264 -x264encopts bitrate=$BITRATE:$PARAMS:$EXTRA_PARAMS:pass=2  




#-flags +loop+mv4 -cmp 800 \
#	   -partitions +parti4x4+parti8x8+partp4x4+partp8x8+partb8x8 \
#	   -me_method hex -subq 7 -trellis 1 -refs 5 -bf 3 \
#	   -flags2 +bpyramid+wpred+mixed_refs+dct8x8 -coder 1 -me_range 16 \
#           -g 800 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -qmin 10\
#	   -qmax 51 -qdiff 4


# MÉTODO 3: con ffmpeg

# MÉTODO DIRECTO:

#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -vpre slow_firstpass -b 900k -bt 900k -threads 0 "$vidout"

#ffmpeg -i "$origen" -acodec libvorbis -ar 44100 -ab 64k -ac 2 -pass 2 -vcodec libx264 -vpre slow -b 900k -bt 900k -threads 0  -y "$vidout"

# MÉTODO POR PARTES PARA EVITAR CORTES EN SONIDO:

#ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 "$vidout".mp3
#pacpl -t ogg "$vidout".mp3 -o mp3  --bitrate 68 --oggqual 0 --outdir .
#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -vpre slow_firstpass -b 900k -bt 900k -threads 0 "$vidout".avi
#ffmpeg -i "$origen" -an -pass 2 -vcodec libx264 -acodec -vpre slow -b 900k -bt 900k -threads 0  -y "$vidout".avi
#mencoder "$vidout".tmp.mkv -ovc copy -oac vorbis -audiofile "$vidout".mp3 -o "$vidoutput"
#ffmpeg -i "$origen" -acodec -acodec libvorbis -ar 44100 -ab 64k -ac 2  kk.x264.mkv
#ffmpeg  -i "$vidout".avi  -i "$vidout".ogg  -acodec copy -vcodec copy  kk.x264.mkv



#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -vpre slow_firstpass -b 900k -bt 900k -threads 0 "$vidout"

#ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libx264 -vpre slow -b 900k -bt 900k -threads 0  -y "$vidout"
