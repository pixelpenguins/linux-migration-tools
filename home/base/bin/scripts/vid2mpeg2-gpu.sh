#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}






for i in *.mkv

do 

	origen="$i"
	cambiaext "$i"
	vidtemp="$namefich""tmp.mkv"
	vidout="$namefich""mpeg4.avi"
	bitrate="915k"


echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k

echo "el bitrate optimo es $bitrateoptimo"
echo "codificando a $bitrateoptimo"

bitrateoptimo="1800k"

# QUITAMOS LOS SUBTÍTULOS

mkvmerge --no-subtitles "$origen" -o "$vidtemp"


ffmpeg -vaapi_device /dev/dri/renderD128 -hwaccel vaapi -hwaccel_output_format vaapi -i "$vidtemp" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -vf 'format=nv12|vaapi,fps=30,hwupload' -threads 0 -r 30 -c:v mpeg2_vaapi -y "$vidout"




rm -rf "$vidtemp"
rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree

done 









for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv  *.dv *.vob *.m4v *.webm

#for i in *.dot

do 

	origen="$i"
	cambiaext "$i"
	vidtemp="$namefich""tmp.mkv"
	vidout="$namefich""mpeg4.avi"
	bitrate="915k"


echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k

echo "el bitrate optimo es $bitrateoptimo"
echo "codificando a $bitrateoptimo"

bitrateoptimo="1800k"



ffmpeg -vaapi_device /dev/dri/renderD128 -hwaccel vaapi -hwaccel_output_format vaapi -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -vf 'format=nv12|vaapi,fps=30,hwupload' -threads 0 -r 30 -c:v mpeg2_vaapi -y "$vidout"




rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree

done 






