#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


vidbitrate=200
vidbitrate=$1

for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv  *.dv *.vob *.m4v
#for i in *.avi
do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""_.mp4"

ffmpeg -i "$origen" -an -pass 1 -vcodec libx264  -b "$vidbitrate"k -bt "$vidbitrate"k -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 "$vidout"

ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 96k -ac 1 -pass 2 -vcodec libx264 -b "$vidbitrate"k -bt "$vidbitrate"k -vf unsharp=5:5:1.0:5:5:1.0 -threads 0  -y "$vidout"

rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree





done 

