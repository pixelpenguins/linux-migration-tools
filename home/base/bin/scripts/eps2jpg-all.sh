#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


for i in *.eps *.ps *.ai
do



	cambiaext "$i"

	echo "convirtiendo archivo: "$i" ..."

	gs -sDEVICE=jpeg -dJPEGQ=100 -dNOPAUSE -dEPSFitPage -dBATCH -dSAFER -r300 -sOutputFile="$namefich"jpg "$i" >> /dev/null

	mogrify -trim -resize 800x600 "$namefich"jpg

	echo

done 
