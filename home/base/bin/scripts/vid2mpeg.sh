#!/bin/bash









# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# ***************************************************************************
# la función printcabecera pinta un pequeño membrete por pantalla.

function printcabecera 
{
	echo "****************************************************************";
	echo "vid2mpeg $version";
	echo "(c) 2010 jEsuSdA & Dámaso";
	echo "Released without warranty under the terms of the GPL LICENSE";
	echo "****************************************************************";
	echo "LAME, FFMPEG and MENCODER are required."
	echo "****************************************************************";
}

# ***************************************************************************
# la función printhelp pinta una pequeña ayuda por pantalla.
function printhelp
{
	echo "Usage:"
	echo "      vid2mpeg option file to convert"
	echo "      or"
	echo "      vid2mpeg file to convert option"

	echo "****************************************************************";
	echo "Options:"
	echo "      NONE  :  Creates a VCD format video."
	echo "      -dvd  :  Creates a DVD format video."
	echo "      -vcd  :  Creates a VCD format video."
	echo "      -svcd :  Creates a SVCD format video."
	echo "      -h    :  Show this help."
	echo "      --help:  Show this help."
	echo "****************************************************************";

	exit
}

# ***************************************************************************
function codecdefault
{
	echo "Using VCD video format..."
	echo "****************************************************************";
	cambiaext "${1}"
	vidout=vcd_$namefich"mpg"

# PAL VCD

#mencoder -oac lavc -ovc lavc -of mpeg -mpegopts format=xvcd -vf scale=352:288,harddup -srate 44100 -af lavcresample=44100 -lavcopts vcodec=mpeg1video:keyint=15:vrc_buf_size=327:vrc_minrate=1152:vbitrate=1152:vrc_maxrate=1152:acodec=mp2:abitrate=224:aspect=16/9 -ofps 25 -o "$vidout" "${1}"

ffmpeg -i "${1}" -target pal-vcd "$vidout"

}


# ***************************************************************************
function codecdvd
{
	echo "Using DVD video format..."
	echo "****************************************************************";
	cambiaext "${1}"
	vidout=dvd_$namefich"mpg"

# PAL DVD

#mencoder -oac lavc -ovc lavc -of mpeg -mpegopts format=dvd:tsaf -vf scale=720:576,harddup -srate 48000 -af lavcresample=48000 -lavcopts vcodec=mpeg2video:vrc_buf_size=1835:vrc_maxrate=9800:vbitrate=5000:keyint=15:vstrict=0:acodec=ac3:abitrate=192:aspect=16/9 -ofps 25 -o "$vidout" "${1}"

ffmpeg -i "${1}" -target pal-dvd "$vidout"

#maybe you can add -aspect 16:9 if the output video does not work well.

}


# ***************************************************************************
function codecsvcd
{
	echo "Using SVCD video format..."
	echo "****************************************************************";
	cambiaext "${1}"
	vidout=svcd_$namefich"mpg"


# PAL SVCD

#mencoder -oac lavc -ovc lavc -of mpeg -mpegopts format=xsvcd -vf scale=480:576,harddup -srate 44100 -af lavcresample=44100 -lavcopts vcodec=mpeg2video:mbd=2:keyint=15:vrc_buf_size=917:vrc_minrate=600:vbitrate=2500:vrc_maxrate=2500:acodec=mp2:abitrate=224:aspect=16/9 -ofps 25 -o "$vidout" "${1}"

ffmpeg -i "${1}" -target pal-svcd -threads 2 "$vidout"

}




clear
printcabecera

if [ $# = "0" ]
then
	
	printhelp
	exit
fi

while [ "$1" != "" ] 
do 

	case $1 in

		--help)  param="-h"
		;;
		-h) param="-h"
		;;
		-dvd) param="-dvd"
		;;
		-vcd) param="-vcd"
		;;
		-svcd) param="-svcd"
		;;
		*) file="$file $1"
		;;
	esac
	shift
done


file=${file:1:${#file}}




case $param in

	-h) printhelp
	;;
	-dvd) codecdvd "${file}"
	;;
	-vcd) codecdefault "${file}"
	;; 
	-svcd) codecsvcd "${file}"
	;;
	*) codecdefault  "${file}"
	# si no se pasa ningún parámetro, usamos la configuración por defecto: (vcd)
	;;
esac


# codifica "${file}"


exit


# more info in: http://www.mplayerhq.hu/DOCS/HTML/en/menc-feat-vcd-dvd.html
