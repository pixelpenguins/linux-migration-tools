#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}

mkdir bak

for i in *.png
do

	cambiaext "$i"

	convert   -quality 85 -transparent-color white -flatten   "$i" "$namefich"jpg
	echo "Imagen $i convertida ..."
	
	mv "$i" bak/

done 
