#!/bin/sh
# SCREENCAST BY JESUSDA
# SISTEMA DE GRABACIÓN DE SCREENCAST CON SOX, FFMPEG Y MENCODER.


fname=`date +"podcast_%Y-%m-%d_%m-%M_%p"`
begindate=`date +"%s"`

formatoaudio=wav


clear

echo "===================================================="
echo 
echo "ACTIVANDO GRABACIÓN DE AUDIO..."
echo 

sox -q -t alsa "hw:1,0"  -t ogg -c1 audio.ogg rate 44100 gain +16 dither -s > /dev/null &
soxpid=`ps -A | grep sox | awk '{print $1}'`

echo "===================================================="
echo 
echo "Pulsa una tecla para terminar"
echo 

read $key
kill -9 $soxpid 2> /dev/null


ffmpeg  -i audio.ogg audio.mp3

mv audio.ogg "$fname".ogg
mv audio.mp3 "$fname".mp3



enddate=`date +"%s"`
timediff=`expr $enddate - $begindate`
mins=`expr $timediff / 60`
secs=`expr $timediff % 60`


echo "===================================================="
echo 
echo "La grabación $fname duró $mins minutos y $secs segundos"
echo 
echo "===================================================="


exit
