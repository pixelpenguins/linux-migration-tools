#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


bitrate=550

for i in *.flv *.avi *.mpg *.mpeg *.webm *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.ogg *.ogm *.ogv  *.dv
do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""x264.mkv"

	echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	bitrateoptimo=900
	echo "el bitrate optimo es $bitrateoptimo"
	echo "codificando a $bitrateoptimo"


	if [ "$(( $bitrateoptimo > $bitrate ))" -eq 1 ]; then
         
			# Forzamos el bitrate por defecto si el bitrate optimo es mayor que éste.
			brdest=$bitrate

     else
			# Si el bitrate optimo es menor que el bitrate por defecto, lo dejamos.
			brdest=$bitrateoptimo

     fi

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	brdest="$brdest"k


#ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -vcodec libx264 -r 30 -preset slow -s 854x480 -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf unsharp=5:5:1.0:5:5:1.0 -tune film -x264-params opencl -threads 0 -y "$vidout"


ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -c:v libx264 -r 30 -vf 'scale=640:360,unsharp=5:5:1.0:5:5:1.0' -tune film -crf 15 -threads 0 -y "$vidout"


rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree


done 

