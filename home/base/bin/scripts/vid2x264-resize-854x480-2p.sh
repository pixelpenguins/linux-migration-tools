#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}






for i in *.mkv

do 

	origen="$i"
	cambiaext "$i"
	vidtemp="$namefich""tmp.mkv"
	vidout="$namefich""x264.mkv"
	bitrate="1650"


echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`


# si el bitrate óptimo resultante es 1100, entonces es muy bajo para esta resolución
# de vídeo, así que lo sustituimos por el bitrate indicado en la variable bitrate,
# que es mayor.

if [ $bitrateoptimo -eq 1100 ]; then  
			bitrateoptimo=$bitrate
fi


	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k



echo "el bitrate optimo es $bitrateoptimo"
echo "codificando a $bitrateoptimo"



# QUITAMOS LOS SUBTÍTULOS

mkvmerge --no-subtitles "$origen" -o "$vidtemp"


#ffmpeg -i "$vidtemp" -an -pass 1 -vcodec libx264 -r 30 -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -s 854x480 -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 "$vidout"

#ffmpeg -i "$vidtemp" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libx264 -r 30 -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -s 854x480 -vf unsharp=5:5:1.0:5:5:1.0 -threads 0  -y "$vidout"



ffmpeg -i "$vidtemp" -an -pass 1 -vcodec libx264 -r 30 -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf 'scale=854:480,unsharp=5:5:1.0:5:5:1.0' -threads 0 "$vidout"

ffmpeg -i "$vidtemp" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libx264 -r 30 -preset slow -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf 'scale=854:480,unsharp=5:5:1.0:5:5:1.0' -threads 0  -y "$vidout"

rm -rf "$vidtemp"
rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree

done 









for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv  *.dv *.vob *.m4v *.webm

do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""x264.mkv"


echo "calculando bitrate"
	# usamos el scrtip bitrate-optimo para calcular el br a usar en la codificación.
	bitrateoptimo=`bitrate-optimo.sh "$origen"`

	# ffmpeg necesita que le pasemos el bitrate acabado en K, por eso le añadimos la K
	bitrateoptimo="$bitrateoptimo"k

echo "el bitrate optimo es $bitrateoptimo"
echo "codificando a $bitrateoptimo"




#ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -r 30 -preset slow -s 854x480 -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf unsharp=5:5:1.0:5:5:1.0 -threads 0 "$vidout"

#ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libx264 -r 30  -preset slow -s 854x480 -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf unsharp=5:5:1.0:5:5:1.0 -threads 0  -y "$vidout"


ffmpeg -i "$origen" -an -pass 1 -vcodec libx264 -r 30 -preset slow  -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf 'scale=854:480,unsharp=5:5:1.0:5:5:1.0' -threads 0 "$vidout"

ffmpeg -i "$origen" -acodec libmp3lame -ar 44100 -ab 128k -ac 2 -pass 2 -vcodec libx264 -r 30  -preset slow  -b:v "$bitrateoptimo" -bt:v "$bitrateoptimo" -vf 'scale=854:480,unsharp=5:5:1.0:5:5:1.0' -threads 0  -y "$vidout"


rm -rf x264_2pass.log.mbtree
rm -rf ffmpeg2pass-0.log
rm -rf x264_2pass.log
rm -rf *.log.tmp
rm -rf *.log.mbtree

done 



