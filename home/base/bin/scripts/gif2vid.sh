#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}

mkdir bak

convert "$1" bak/some%05d.jpg

ffmpeg -f image2 -i bak/some%05d.jpg video.mpg


#rm -rf bak/
