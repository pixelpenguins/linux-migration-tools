#!/bin/bash 


if [ $# = "0" ]
then
	
	echo "USAGE: render.sh <size>"
	exit
fi

for i in *.svg
do

    str=$i
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}


	#rsvg -h $1 -w $1 $i "$namefich"png

	inkscape -z $i --export-width=$1 -e "$namefich"png

done
