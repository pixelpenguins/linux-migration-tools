#!/bin/bash


for i in *.jpg *.jpeg
do


# IMAGEN
IMAGEN="$i"

# TAMAÑO DEL BORDE NEGRO INFERIOR
margen="30"

# LEEMOS DE TECLADO EL TÍTULO DE LA IMAGEN
echo -n "Título de la imagen $i: "
read caption


# AVERIGUAMOS EL TAMAÑO DE LA IMAGEN
DIMENSIONES=`identify -format "%wx%h" "$i"`
#echo "Dimensiones: $DIMENSIONES"

# CALCULAMOS LAS POSICIONES PARA COLOCAR BORDE Y TEXTO
x=`echo $DIMENSIONES | awk -F"x" '{print $1}'`
y=`echo $DIMENSIONES | awk -F"x" '{print $2}'`
echo $x
echo $y
# el limite sera el tamaño de y menos 30

let lim=$y-$margen
#echo $lim

# y ya utilizo convert para generar la nueva imagen añadiendo un rectangulo negro y el texto

convert "$i" -gravity SouthWest -fill black -draw "rectangle 0,$lim $x,$y" -fill white -pointsize 20 -font Helvetica -fill white -draw "text 13 3 \"$caption\"" lgn_"$i".jpg

done

exit

