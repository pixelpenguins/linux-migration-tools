#!/bin/bash

# carpetatemp=$$
# 
# mkdir $carpetatemp


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


for i in *.pdf
do

	echo -n "Procesando $i" 

	cambiaext "$i"
	echo -n "."

	
	gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -sOutputFile="$namefich"optimized.pdf "$i"

#Si usás /screen en vez de /ebook queda legible en una pantalla, y más reducido.

	echo -n "."
	echo " OK!"

done
