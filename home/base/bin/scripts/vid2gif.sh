#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.ogm *.ogv *.dv *.vob
do 

	origen="$i"
	cambiaext "$i"
	vidout=$namefich"gif"

	mkdir temp 2> /dev/null
	cd temp

	mplayer -vo png  -nosound ../"$origen"

	# aligerando frames (eliminando frames pares)...
	rm *0.png *2.png *4.png *6.png *8.png
	
	# limpiar frames inservibles:
	echo -n "Elimine los archivos con frames a desechar y pulse una tecla..."
	read $key

	# generando gif...
	convert *.png "$vidout"

	# optimizar gif
	gifsicle -O3 --colors 200 "$vidout" > ../"$vidout"


	rm *.png
	cd ..


#	ffmpeg -i "$origen" -pix_fmt rgb24 "$vidout"



# Otro sencillo truco que nos permitirá transformar un video en una imagen GIF con movimiento. El comando como siempre es muy sencillo:

# ffmpeg -i video.mpeg imagen.gif
# Sin embargo, si te llega a salir el error [gif @ 0xb7f2b208]ERROR: gif only handles the rgb24 pixel format, solo debes modificar un poco el comando:

# ffmpeg -i video.mpeg -pix_fmt rgb24 imagen.gif
# Con esto le indicamos a ffmpeg que el formato de los pixeles será rgb24.


done 

rm "*.gif"
rm "*.dvgif"
rm -rf temp

