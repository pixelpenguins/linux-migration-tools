#!/bin/bash

mkdir s5-pics

for i in *.png *.jpg *.gif *.jpeg
do
	echo "convirtiendo $i"
	convert -resize x400  -quality 70 "$i" s5-pics/"$i"_s5.jpg
	echo "OK!"
done

