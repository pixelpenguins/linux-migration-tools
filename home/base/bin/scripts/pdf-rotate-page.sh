#!/bin/bash

# primer parámetro = nombre archivo pdf.
# segundo parámetro = página.


echo " pdf-rotatepage.sh nombre-archivo página"

file=$1
pagina=$2
paginaanterior=`expr $pagina - 1`
paginasiguiente=`expr $pagina + 1`


pdftk "$file" cat 1-"$paginaanterior" "$pagina"east "$paginasiguiente"-end output pdf-rotado.pdf


echo "..OK!"
