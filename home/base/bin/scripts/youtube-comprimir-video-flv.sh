#!/bin/bash

for i in *.avi
do
mv "$i" "$i.bak"

nice -5 mencoder "$i.bak" -vf hqdn3d,scale=425:350,unsharp=l3x3:0.75:c3x3:0.75 -ovc xvid -xvidencopts bitrate=1000  -oac mp3lame -lameopts preset=96 -o "$i"

# ffmpeg -i "$i" "$i".flv
# ffmpeg -i "$i" -b 700000 "$i".flv
ffmpeg -i "$i" -b 700000 "$i".flv


# mencoder "$i.bak" -vf hqdn3d,scale=320:240,unsharp=l3x3:0.75:c3x3:0.75 -ovc xvid -xvidencopts bitrate=800  -oac mp3lame -lameopts preset=96 -o "$i"

done