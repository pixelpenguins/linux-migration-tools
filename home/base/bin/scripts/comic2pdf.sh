#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



QUALITY=75
tecla=a



echo "PREPARANDO..."






for i in *.cbr
do


	origen="$i"
	cambiaext "$i"
	out=$namefich"pdf"
	out2=$namefich"ps"

	echo "ABRIENDO $i"
	mkdir comic 2> /dev/null
	
# 	DESCOMPRIMIR EN COMIC
	echo "DESCOMPRIMIENDO $i"
	unrar x -inul "$i" comic 2> /dev/null


# 	ENTRAR EN COMIC
	echo "ENTRANDO EN CÓMIC..."
	cd comic
	#ls
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS
	echo "AGRUPANDO IMÁGENES..."

	mkdir imagespdf 

	find . -type f -iname "*.jpg"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.jpeg"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.JPG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.JPEG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.png"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.PNG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.gif"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.GIF"  -exec cp {} ./imagespdf \; 2> /dev/null




	echo "CREANDO PDF..."

	cd imagespdf 

	for i in *.jpg
	do

	#	convert *.* ../../"$out"
	convert $i $i.pdf
	echo -n "."
	done

	pdfjoin *.pdf -o ../../"$out"

	cd ../..

	echo -n "OPTIMIZANDO PDF..."

	pdf2ps "$out" "$out2"
	echo -n "."
	ps2pdf "$out2" "$out"
	echo "."

	echo "TERMINANDO... $i"

	rm -rf comic
	rm "$out2"


done


for i in *.cbz
do


	origen="$i"
	cambiaext "$i"
	out=$namefich"pdf"
	out2=$namefich"ps"

	echo "ABRIENDO $i"
	mkdir comic 2> /dev/null
	
# 	DESCOMPRIMIR EN COMIC
	echo "DESCOMPRIMIENDO $i"
	unzip -qq "$i" -d comic 2> /dev/null


# 	ENTRAR EN COMIC
	echo "ENTRANDO EN CÓMIC..."
	cd comic
	#ls
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS
	echo "AGRUPANDO IMÁGENES..."

	mkdir imagespdf 

	find . -type f -iname "*.jpg"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.jpeg"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.JPG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.JPEG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.png"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.PNG"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.gif"  -exec cp {} ./imagespdf \; 2> /dev/null
	find . -type f -iname "*.GIF"  -exec cp {} ./imagespdf \; 2> /dev/null




	echo "CREANDO PDF..."

	cd imagespdf 

	for i in *.jpg
	do

	#	convert *.* ../../"$out"
	convert $i $i.pdf
	echo -n "."
	done

	pdfjoin *.pdf -o ../../"$out"

	cd ../..

	echo -n "OPTIMIZANDO PDF..."

	pdf2ps "$out" "$out2"
	echo -n "."
	ps2pdf "$out2" "$out"
	echo "."

	echo "TERMINANDO... $i"

	rm -rf comic
	rm "$out2"


done


echo "TERMINADO... ;)"
echo "OJO! SÓLO CONVIERTE A PDF LAS IMÁGENES JPG DE LOS CÓMICS"

