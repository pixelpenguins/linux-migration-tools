#!/bin/bash

# Este script está pensado para optimizar el proceso de recodificación de los vídeos
# Lo que hace es analizar el bitrate del vídeo y devolverlo si es menor que el bitrate
# por defecto.

# La idea es no perder el tiempo recomprimiendo un vídeo a un bitrate mayor que el
# bitrate de origen, pues es seguro que dará un archivo de mayor tamaño como resultado.


brdefault="1150"
brorig="2000"

video="$1"

#echo "Analizando video $video"

#ffprobe 01.avi > temp.txt
mplayer -vo null -ao null -identify -frames 0 "$video" 2> /dev/null | grep kbps | grep VIDEO > temp.txt


brorig=`cat temp.txt | tr -s ' ' ' ' | cut -d ' ' -f 7 | cut -d '.' -f 1`

rm -rf temp.txt

#echo "El bitrate por defecto es: $brdefault"
#echo "El vídeo $video tiene bitrate: $brorig"


if [ $brorig -eq 0 ]; then
         
			# El tag de bitrate del vídeo no se pudo optener y da 0
			brorig="2000"
        fi




if [ "$(( $brdefault > $brorig ))" -eq 1 ]; then
         
			# El vídeo tiene un bitrate menor que el bitrate por defecto
			brdest=$brorig

        else
			# El bitrate del vídeo es mayor que el bitrate por defecto
			brdest=$brdefault

        fi


#echo "El bitrate menor es: $brdest"


echo "$brdest"
exit





