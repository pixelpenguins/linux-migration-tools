#!/bin/bash


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



for i in *.tif
do
	origen="$i"
	cambiaext "$i"
	out="$namefich""jpg"


convert "$i" "$out"

done
