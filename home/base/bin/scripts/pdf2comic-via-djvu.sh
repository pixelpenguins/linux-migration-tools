#!/bin/bash

# REQUIERE:
#
# pdf2djvu , imagemagick, rar


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


mkdir bak


for i in *.pdf 
do

	origen="$i"
	cambiaext "$i"
	out=$namefich

	cp "$origen" bak/

	pdf2djvu "$origen" -o "$namefich"djv

	ddjvu -format=pdf -quality=100 -verbose "$namefich"djv "$namefich"pdf

	echo "$origen convertido satisfactoriamente..."

done

echo "Convirtiendo pdfs a comic..."
/home/jesusda/base/bin/scripts/pdf2comic.sh

echo "Optimizando cómics..."
/home/jesusda/base/bin/scripts/comic-recomp-jpg.sh
