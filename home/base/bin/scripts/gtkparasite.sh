#!/bin/bash

echo "Escriba aplicación a ejecutar..."
read app


gsettings set org.gtk.Settings.Debug enable-inspector-keybinding true
GTK_DEBUG=interactive "$app"

echo "======================================="
echo "pulsa Ctr+Mays+I o Ctr+Mays+D "
echo "======================================="

gsettings set org.gtk.Settings.Debug enable-inspector-keybinding false


# old gtkparasite
# GTK_MODULES=gtkparasite $app
