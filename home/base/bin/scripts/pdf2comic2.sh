#!/bin/bash

# REQUIERE:
#
# pdftk , imagemagick, rar


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



for i in *.pdf 
do

	origen="$i"
	cambiaext "$i"
	out=$namefich"cbr"


#	pdfimages -j "$origen"  pic

#	for a in *.ppm 
#	do
#		convert "$a" "$a".jpg
#		rm -rf "$a"
#	done

#	for a in *.pbm 
#	do

#		convert -monochrome -density 200 -depth 1 "$a" "$a".png
#		rm -rf "$a"
#	done


PdfNumeroPaginas=`pdftk "$origen" dump_data output 2> /dev/null | grep "NumberOfPages" | cut -d ' ' -f 2`

num=0

while [ $num -le $PdfNumeroPaginas ]; do

   convert -density 300 "$origen"[$num] pic_$num.png

    let num=$num+1
done




	rar a "$out" *.jpg *.png
	rm -rf *.jpg *.png




done
