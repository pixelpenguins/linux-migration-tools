#!/bin/sh

GNASH="/usr/bin/gtk-gnash"
SCROT="/usr/bin/scrot"
OUTPUTFORMAT="jpg"

if [ $# -ne 1 ]
then
  echo "error, usage: $(basename $0) file.swf"
  exit 1
else
  $GNASH --fullscreen $1 &
  GNASHPID=$!
  $SCROT -d 1 $1.$OUTPUTFORMAT
  kill $GNASHPID
fi