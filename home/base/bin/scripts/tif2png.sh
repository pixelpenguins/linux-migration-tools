#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}

mkdir bak

for i in *.tif
do

	cambiaext "$i"

	convert "$i" "$namefich"png
	
	mv "$i" bak/

done 
