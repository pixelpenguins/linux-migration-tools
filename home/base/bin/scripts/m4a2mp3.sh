#!/bin/bash
# Convertidor de m4a a mp3 por jesusda


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}



# for f1 in *.m4a;
# do
# 	f2=`echo $f1 | cut -d '.' -f 1`.mp3;
# 	
# 	mplayer -ao pcm "$1"
# 	lame -h -b 192 "audiodump.wav" "$2"
# 	rm audiodump.wav
# done 

#  FORMA 1
# 
# for i in *.m4a;
# do
# 	mplayer -ao pcm "$i"
# 	nice lame -h -b 192 "audiodump.wav" "$i".mp3
# 	rm audiodump.wav
# done 

# FORMA 2

#!/bin/bash
# for i in *.m4a; do
# 	echo "Converting: ${i%.m4a}.mp3"
# 	nice faad -o - "$i" | lame - "${i%.m4a}.mp3"
# done

#  FORMA 3

 for i in *.m4a;
 do

	origen="$i"
	cambiaext "$i"
	destino="$namefich""mp3"

 	ffmpeg -i "$origen" "$destino"

 done




