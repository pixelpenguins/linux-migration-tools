#!/bin/bash



function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


#algoritmo="convert -monochrome -density 400 -depth 4 "
#algoritmo="convert -colorspace Gray -modulate 100,00,100 -depth 3 -colors 8 -normalize -density 800 "
# algoritmo="convert -normalize -colorspace Gray -modulate 100,00,100 -depth 3 -colors 8 -normalize  -density 200 "

#algoritmo="convert -normalize -colorspace Gray -modulate 115,00,200 -colors 8 -normalize  -density 200 "

#algoritmo="mogrify -format png -auto-level -normalize -modulate 105,00,00 -colorspace gray -colors 9 -auto-level  -normalize  "

algoritmo="convert -format png -auto-level -normalize -modulate 105,00,00 -colorspace gray -colors 9 -auto-level  -normalize "


QUALITY=75
tecla=a



echo "PREPARANDO..."

mkdir bak 2> /dev/null

echo "CAMBIANDO EXTENSIÓN A LOS COMICS..."

for i in *.cbr
do
	origen="$i"
	cambiaext "$i" 2> /dev/null
	out=$namefich"rar"
	mv "$i" "$out" 2> /dev/null

done

for i in *.cbz
do
	origen="$i"
	cambiaext "$i" 2> /dev/null
	out=$namefich"zip"
	mv "$i" "$out" 2> /dev/null

done


minusculas.sh > /dev/null 2> /dev/null 
minusculas.sh > /dev/null 2> /dev/null


echo -n "Deseas hacer un backup de los cómics? (Y/N):"
read tecla

if [ $tecla = y ]; then

	cp *.zip bak 2> /dev/null
	cp *.rar bak 2> /dev/null

fi







for i in *.rar 
do


	origen="$i"
	cambiaext "$i"
	out=$namefich"cbz"


	echo "ABRIENDO $i"
	mkdir comic 2> /dev/null

# 	DESCOMPRIMIR EN COMIC
	echo "DESCOMPRIMIENDO $i"
	unrar x -inul "$i" comic 2> /dev/null


# 	ENTRAR EN COMIC
	echo "ENTRANDO EN CÓMIC..."
	cd comic
	#ls


#	Cambio de permisos:
	chmod 777 *

	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS
	echo "AGRUPANDO IMÁGENES..."

	find . -type f -iname "*.jpg"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.jpeg"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.JPG"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.JPEG"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.png"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.PNG"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.gif"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.GIF"  -exec cp {} . \; 2> /dev/null

#	Quitar morralla:

	find . -type f -iname "*.db"  -exec rm -rf {} \; 2> /dev/null
	find . -type f -iname "*hentairulesbanner*"  -exec rm -rf {} \; 2> /dev/null
	rm -rf "Hentai Manga and Doujin Downloads ! Free Hentai Manga ! Hentai Manga Magazines ! Hentai Anime Downloads_files"
	rm -rf "Download Hentai Movies I Free Hentai Download_files"
	rm -rf 403.gif
	rm -rf 403*.gif
	rm -rf *Creditos*
	rm -rf *creditos*
	rm -rf *credits*
	rm -rf *Reclutamiento*
	rm -rf *reclutamiento*



# 	EN ESTA PARTE YA NO SE REQUIERE LA AYUDA DEL USUARIO

	mkdir 1bit
	chmod 777 *

	echo -n "SEPARANDO LAS IMÁGENES A COLOR DE LAS DE BLANCO Y NEGRO..."

	for b in *.jpg *.png *.gif
	do

		# el siguiente comando chequea la imagen y muestra una serie de datos 
		# referentes a la probabilidad de que haya color en la imagen chequeada.
		# lo muestra así:
		# avg=42445.3 peak=65535

		# info=`convert "$b"  -colorspace HSB -channel G -separate +channel -format 'avg=%[mean] peak=%[maximum]' info:`

		# $info tendría ahora la cadena resultado de la comparación: avg=42445.3 peak=65535
		# tendríamos que tratar de sacar sólo el número de la primera columna:

		info="0"
		info=`convert "$b" -colorspace HSB -channel G -separate +channel -format 'avg=%[mean] peak=%[maximum]' info: 2> /dev/null | cut -d ' ' -f1 | cut -d '=' -f2 | cut -d '.' -f1 2> /dev/null`

		# partiendo de avg=42445.3 peak=65535
		# con cut -d ' ' -f1 nos quedamos sólo con avg=42445.3
		# con cut -d '=' -f2 nos quedaríamos con 42445.3
		# con cut -d '.' -f1 nos quedamos con la parte entera, es decir 42445
		# que es justo lo que necesitamos ;)

		# Ahora, si el valor de $info es pequeño -> imagen en bn.
		# si el valor de $info es grande -> imagen en color.
		# a las imágenes en color les damos un tratamiento (para que no pierdan el color) y a las de bn
		# les damos otro.

		if [ "$info" -gt "9000" ] 2> /dev/null;
		then

			# si la imagen es en color, la movemos a la carpeta 1bit.

			mv "$b" ./1bit/
			echo -n .

		fi


	done

	echo



# 	CONVERTIR LAS IMÁGENES DE COLOR A MENOS CALIDAD

	chmod 777 *.* 2> /dev/null
	minusculas.sh > /dev/null 2> /dev/null

	echo -n "RECOMPRIMIENDO IMÁGENES EN COLOR..."

	cd 1bit

	for b in *.jpg *.png *.gif
	do
		convert -quality $QUALITY "$b" "$b".jpg 2> /dev/null
		rm "$b" 2> /dev/null
		echo -n .
	done

	cd ..

# 	CONVERTIR LAS IMÁGENES A 1Bit CALIDAD

	echo
	echo -n "RECOMPRIMIENDO IMÁGENES EN BLANCO Y NEGRO..."



	
	for b in *.jpg *.png *.gif
	do

		$algoritmo "$b" "$b".png  2> /dev/null
		mv  "$b".png 1bit/ 2> /dev/null
		echo -n .
	done







	echo
	echo "OPTIMIZANDO IMÁGENES... (esto tardará un ratico...)"


# 	Quitamos datos EXIF de las imágenes
	mogrify -strip 1bit/*.png 2> /dev/null
	mogrify -strip 1bit/*.jpg 2> /dev/null

# 	Optimizamos los png's
	optipng -quiet 1bit/*.png 


# 	SALIMOS DE COMIC Y COMPRIMIMOS EN zip



	cd ..

	echo "CERRRANDO $i"

	zip -qq "$out" comic/1bit/*
	rm -rf comic
	rm -rf /tmp/magick*

done

















# 	HACEMOS LO MISMO PARA LOS ZIP

for i in *.zip
do



	origen="$i"
	cambiaext "$i"
	out=$namefich"cbz"
	
	echo "ABRIENDO $i"
	mkdir comic 2> /dev/null


# 	DESCOMPRIMIR EN COMIC
	unzip -qq "$i" -d comic 2> /dev/null

# 	ENTRAR EN COMIC
	echo "ENTRANDO EN CÓMIC..."
	cd comic
	#ls


#	Cambio de permisos:
	chmod 777 *
	
# 	SACAR TODAS LAS IMÁGENES DE LAS POSIBLES SUBCARPETAS
	echo "AGRUPANDO IMÁGENES..."

	find . -type f -iname "*.jpg"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.jpeg"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.JPG"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.JPEG"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.png"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.PNG"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.gif"  -exec cp {} . \; 2> /dev/null
	find . -type f -iname "*.GIF"  -exec cp {} . \; 2> /dev/null

#	Quitar morralla:

	find . -type f -iname "*.db"  -exec rm -rf {} \; 2> /dev/null
	find . -type f -iname "*hentairulesbanner*"  -exec rm -rf {} \; 2> /dev/null
	rm -rf "Hentai Manga and Doujin Downloads ! Free Hentai Manga ! Hentai Manga Magazines ! Hentai Anime Downloads_files"
	rm -rf "Download Hentai Movies I Free Hentai Download_files"
	rm -rf 403.gif
	rm -rf 403*.gif
	rm -rf *Creditos*
	rm -rf *creditos*
	rm -rf *credits*
	rm -rf *Reclutamiento*
	rm -rf *reclutamiento*




# 	EN ESTA PARTE YA NO SE REQUIERE LA AYUDA DEL USUARIO

	mkdir 1bit 
	chmod 777 *
	echo -n "SEPARANDO LAS IMÁGENES A COLOR DE LAS DE BLANCO Y NEGRO..."

	for b in *.jpg *.png *.gif
	do

		# el siguiente comando chequea la imagen y muestra una serie de datos 
		# referentes a la probabilidad de que haya color en la imagen chequeada.
		# lo muestra así:
		# avg=42445.3 peak=65535

		# info=`convert "$b"  -colorspace HSB -channel G -separate +channel -format 'avg=%[mean] peak=%[maximum]' info:`

		# $info tendría ahora la cadena resultado de la comparación: avg=42445.3 peak=65535
		# tendríamos que tratar de sacar sólo el número de la primera columna:

		info="0"
		info=`convert "$b" -colorspace HSB -channel G -separate +channel -format 'avg=%[mean] peak=%[maximum]' info: 2> /dev/null | cut -d ' ' -f1 | cut -d '=' -f2 | cut -d '.' -f1 2> /dev/null`

		# partiendo de avg=42445.3 peak=65535
		# con cut -d ' ' -f1 nos quedamos sólo con avg=42445.3
		# con cut -d '=' -f2 nos quedaríamos con 42445.3
		# con cut -d '.' -f1 nos quedamos con la parte entera, es decir 42445
		# que es justo lo que necesitamos ;)

		# Ahora, si el valor de $info es pequeño -> imagen en bn.
		# si el valor de $info es grande -> imagen en color.
		# a las imágenes en color les damos un tratamiento (para que no pierdan el color) y a las de bn
		# les damos otro.

		if [ "$info" -gt "10" ] 2> /dev/null;
		then

			# si la imagen es en color, la movemos a la carpeta 1bit.

			mv "$b" ./1bit/
			echo -n .

		fi


	done

	echo



# 	CONVERTIR LAS IMÁGENES DE COLOR A MENOS CALIDAD

	chmod 777 *.* 2> /dev/null
	minusculas.sh > /dev/null 2> /dev/null

	echo -n "RECOMPRIMIENDO IMÁGENES EN COLOR..."

	cd 1bit

	for b in *.jpg *.png *.gif
	do
		convert -quality $QUALITY "$b" "$b".jpg 2> /dev/null
		rm "$b" 2> /dev/null
		echo -n .
	done

	cd ..

# 	CONVERTIR LAS IMÁGENES A 1Bit CALIDAD

	echo
	echo -n "RECOMPRIMIENDO IMÁGENES EN BLANCO Y NEGRO..."



	
	for b in *.jpg *.png *.gif
	do

		$algoritmo "$b" "$b".png  2> /dev/null
		mv  "$b".png 1bit/ 2> /dev/null
		echo -n .
	done







	echo
	echo "OPTIMIZANDO IMÁGENES... (esto tardará un ratico...)"


# 	Quitamos datos EXIF de las imágenes
	mogrify -strip 1bit/*.png 2> /dev/null
	mogrify -strip 1bit/*.jpg 2> /dev/null

# 	Optimizamos los png's
	optipng -quiet 1bit/*.png 


# 	SALIMOS DE COMIC Y COMPRIMIMOS EN zip



	cd ..

	echo "CERRRANDO $i"

	zip -qq "$out" comic/1bit/* 2> /dev/null
	rm -rf comic

done


echo "TERMINADO... ;)"



