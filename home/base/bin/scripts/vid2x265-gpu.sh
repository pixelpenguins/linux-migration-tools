#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
# vidbitrate=6000

#for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.rm *.ogg *.ogm *.ogv  *.dv *.webm

for i in *.mp4

do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""x265.mkv"


# ffmpeg -i "$origen"  -vcodec libx265 -acodec libvorbis -ar 44100 -ab 96k -ac 2 "$vidout"


#ffmpeg -vaapi_device /dev/dri/renderD128 -hwaccel vaapi -hwaccel_output_format vaapi -i "$origen"  -acodec libvorbis -ar 44100 -ab 96k -ac 2 -vf 'format=nv12|vaapi,hwupload' -threads 0  -r 30  -c:v h264_vaapi "$vidout"


#ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi -vaapi_device /dev/dri/renderD128  -i "$origen" -vf 'format=nv12|vaapi,hwupload' -threads 0 -acodec copy -c:v hevc_vaapi -qp:v 23  "$vidout"

ffmpeg -hwaccel vaapi -i "$origen" -vaapi_device /dev/dri/renderD128 -c:v vp8_vaapi -c:a copy -f webm "$vidout".webm


done 

