#!/bin/bash

pdftk $1 burst


for i in pg*.pdf
do

	pdftops $i $i.ps
	pstoedit $i.ps -f plot-svg $i.svg
done
