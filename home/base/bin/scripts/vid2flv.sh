#!/bin/bash


# **************************************************************************
# Esta función recibe en $1 un nombre de fichero
# Devuelve en namefich ese mismo nombre pero sin extension.
# ejemplo:
#	cambiaext pepito.grillo.avi
#	namefich=pepito.grillo.

function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}


# vidscale=320x240;
# vidbitrate=6000

for i in *.flv *.avi *.mpg *.mpeg *.rmvb *.wmv *.mkv *.divx *.mp4 *.mov *.rm *.ogg *.ogm  *.dv
do 

	origen="$i"
	cambiaext "$i"
	vidout="$namefich""flv"

#	mv "$origen" "$origen"".bak"




	# PARA VERSIONES MODERNAS

# mencoder "$origen" -o "$vidout" -ovc lavc -lavcopts vcodec=flv:vbitrate=700:mbd=2:mv0:trell:v4mv:cbp:last_pred=3   -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1


ffmpeg -i "$origen" -ab 56000 -ar 44100 -b 700000 -r 15 -f flv "$vidout"

#mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts  ref=2:bframes=2:subme=6:mixed-refs=0:weightb=0:trellis=0:8x8dct=0 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1


#  mencoder -forceidx -of lavf -oac mp3lame -lameopts abr:br=56 -srate 22050 -ovc lavc -lavcopts vcodec=flv:vbitrate=250:mbd=2:mv0:trell:v4mv:cbp:last_pred=3 -vf scale=360:240 -o $1.flv $1


	# PARA VERSIONES ANTIGUAS DE MENCODER

# mencoder "$origen" -o "$vidout" -ovc x264 -x264encopts subq=5:8x8dct:frameref=2:bframes=3:b_pyramid:weight_b:bitrate=1000 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1



	# SI TODO FALLA, ENTONCES PUEDES USAR FFMPEG

# 	ffmpeg -i "$origen" -acodec libmp3lame -aq 100 -vcodec libx264 -vpre hq -crf 22 -threads 0 "$vidout"


	#Very high quality	
	#subq=6:partitions=all:8x8dct:me=umh:frameref=5:bframes=3:b_pyramid=normal:weight_b

	#High quality (45fps)
	#subq=5:8x8dct:frameref=2:bframes=3:b_pyramid=normal:weight_b

	#Fast
	#subq=4:bframes=2:b_pyramid=normal:weight_b


#mencoder "$origen" -o "$vidout" -ovc xvid -xvidencopts bitrate=1000 -oac mp3lame -lameopts br=128:q=7:aq=9:cbr:mode=1


# Two-Pass Example
# So if you wanted to encode using two-pass VBR, the command line would be something like:
# 
# ffmpeg -i INPUT -an -pass 1 -vcodec libx264 -vpre fastfirstpass -b BIT_RATE -bt BIT_RATE -threads 0 OUTPUT.mp4
# 
# Note: Use of -vpre fastfirstpass is recommended for first passes as it reduce the encoding time for the first pass while having minimal to no noticable impact on the resulting stream. Also, we don’t encode the audio in the first pass because we will not be using the data that was output.
# 
# ffmpeg -i INPUT -acodec libfaac -ab 128k -pass 2 -vcodec libx264 -vpre hq -b BIT_RATE -bt BIT_RATE -threads 0 OUTPUT.mp4
# 
# Note: We use -vpre hq in the second pass to obtain a high quality result.





done 

