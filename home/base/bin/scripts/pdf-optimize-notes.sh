#!/bin/bash


function cambiaext {
    str=$1
    ext=`echo ${str:(-5)} | cut -d . -f 2`
    len_ext=${#ext}
    len_cad=${#str}
    titulo=$[len_cad-len_ext]
    namefich=${str:0:($titulo)}
}

# obtenemos la ruta desde la que se está ejecutando el script.
mydir="${0%/*}"

echo $mydir

for i in *.pdf 
do

	origen="$i"
	cambiaext "$i"
	out=$namefich"noteshrink.pdf"
	dirtemp="pdftemp"

	mkdir "$dirtemp"
	cd "$dirtemp"

	pdfimages -j ../"$origen"  pic

	for a in *.ppm 
	do
		convert "$a" "$a".jpg
		rm -rf "$a"
	done

	for a in *.pbm 
	do

		convert -monochrome -density 200 -depth 1 "$a" "$a".png
		rm -rf "$a"
	done


	$mydir/noteshrink.py *.jpg *.png

	rm -rf output.pdf
	rm -rf *.jpg
	$mydir/img2pdf.sh

	rm -rf *.png

	pdfjoin *.pdf -o output.pdf

	
	

	cp output.pdf ../"$out"

	cd ..

	rm -rf "$dirtemp"


echo $mydir

done
